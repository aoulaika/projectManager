import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import {
  CompleterData,
  CompleterService,
  RemoteData,
  LocalData,
  CompleterItem
} from "ng2-completer";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";

import { ToastComponent } from "../../shared/toast/toast.component";
import { CustomerService } from "../../services/customer.service";
import { ProjectService } from "../../services/project.service";
import { WebSocketService } from "../../services/web-socket.service";
import { UserService } from "../../services/user.service";
import {AuthService} from '../../services/auth.service';

@Component({
  selector: "app-new-project",
  templateUrl: "./new-project.component.html",
  styleUrls: ["./new-project.component.scss"]
})
export class NewProjectComponent implements OnInit {
  customers: any[];
  dataService: RemoteData;
  ownerUsers: LocalData;
  serviceUsers: LocalData;
  managerUsers: LocalData;
  stakeholders = { subsidiary: "", service: "", manager: "" };
  newProjectForm: FormGroup;
  typeValues = [
    { name: "4D", selected: false, id: 1 },
    { name: "4D Mobile", selected: false, id: 2 },
    { name: "Mobile", selected: false, id: 3 },
    { name: "Wakanda", selected: false, id: 4 },
    { name: "Other", selected: false, id: 5 }
  ];
  files: any[] = [];
  showTechnologies = false;
  currentUser = this.auth.currentUser;
  selectedSubsidiary;
  selectedService;
  selectedManager;
  @ViewChild("attachments") attachments;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    public projectService: ProjectService,
    public userService: UserService,
    private customerService: CustomerService,
    private completerService: CompleterService,
    private webSocketService: WebSocketService,
    private auth: AuthService
  ) {
    this.dataService = completerService.remote(null, "name", "name");
    this.dataService.urlFormater(term => {
      return `/api/customers?query=${term}`;
    });
    this.dataService.dataField("customers");
  }

  ngOnInit() {

    this.newProjectForm = this.fb.group(
      {
        name: ["", Validators.required],
        description: [""],
        customer: ["", Validators.required],
        language: [""],
        technologies: [""],
        stakeholders: this.fb.group({
          subsidiary: [""],
          service: [""],
          manager: [""]
        }),
        types: this.buildTypes(),
        notes: this.fb.group({
          note: [""]
        })
      },
      { validator: this.isTechnologiesRequired }
    );

    switch (this.auth.currentUser.role) {
      case "subsidiary":
          this.stakeholders.subsidiary = this.currentUser._id;
          this.selectedSubsidiary = this.currentUser.firstName + " "+ this.currentUser.firstName;
          break;
      case "manager":
          this.stakeholders.manager = this.currentUser._id;
          this.selectedManager = this.currentUser.firstName + " "+ this.currentUser.firstName;
          break;
      case "service":
          this.stakeholders.service = this.currentUser._id;
          this.selectedService = this.currentUser.firstName + " "+ this.currentUser.firstName;
          break;
    }
    this.getUsers();
  }

  newProject() {
    const project = { ...this.newProjectForm.value };
    if (
      Array.isArray(project.notes) ||
      (project.notes && (!project.notes.note || project.notes.note === ""))
    ) {
      delete project.notes;
    } else {
      project.notes = [project.notes];
    }
    const types = this.newProjectForm.get("types").value;
    project.types = this.typeValues
      .filter((item, index) => types[index])
      .map(item => item.name);
    project.stakeholders = { ...this.stakeholders };
    project.startDate = new Date();
    this.projectService
      .addProject(project)
      .toPromise()
      .then(data => {
        this.webSocketService.emitEventOnProjectCreated(project.name);
        if (this.files.length) {
          return Promise.resolve(data._id);
        }
        return;
      })
      .then(projectId => {
        this.uploadAttachments(projectId);
      })
      .catch(err => console.log(err));
  }

  updateSkateholders(data: { field: string; event: CompleterItem }) {
    this.stakeholders[data.field] = data.event.originalObject["_id"];
  }

  private uploadAttachments(projectId) {
    if (!projectId) {
      this.router.navigate(["/projects"]);
      return;
    }
    const fd = new FormData();
    for (let i = 0; i < this.files.length; i++) {
      fd.append("attachments", this.files[i], this.files[i].name);
    }
    this.projectService.upload(projectId, fd).subscribe(
      data => {
        this.router.navigate(["/projects"]);
      },
      err => console.log(err)
    );
  }

  private buildTypes() {
    const arr = this.typeValues.map(type => {
      return this.fb.control(type.selected);
    });
    return this.fb.array(arr);
  }

  updateFiles(e) {
    const input = e.target;
    for (let i = 0; i < input.files.length; i++) {
      this.files.push(input.files[i]);
    }
  }

  remove(index) {
    this.files.splice(index, 1);
  }

  private getUsers() {
    this.userService.getAll({ admin: false }).subscribe(
      data => {
        const ownerData = data.filter(user => user.role == "subsidiary" && user.subsidiary == this.auth.currentUser.subsidiary);
        this.ownerUsers = this.completerService.local(
          ownerData,
          "firstName,lastName",
          "firstName,lastName"
        );
        const serviceData = data.filter(user => user.role == "service");
        this.serviceUsers = this.completerService.local(
          serviceData,
          "firstName,lastName",
          "firstName,lastName"
        );
        const managerData = data.filter(user => user.role == "manager");
        this.managerUsers = this.completerService.local(
          managerData,
          "firstName,lastName",
          "firstName,lastName"
        );
      },
      error => {
        console.error(error);
      }
    );
  }

  private isTechnologiesRequired = (AC: AbstractControl) => {
    const { value } = AC.get("types");
    const technologies = AC.get("technologies").value;
    if (value.slice(-1).pop()) {
      this.showTechnologies = true;
      if (technologies.trim() === "") {
        AC.get("technologies").setErrors({ required: true });
      }
    } else {
      this.showTechnologies = false;
      AC.get("technologies").setErrors(null);
    }
  };

  get types(): FormArray {
    return this.newProjectForm.get("types") as FormArray;
  }
}
