import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "project-validations",
  templateUrl: "./validations.component.html",
  styleUrls: ["./validations.component.scss"]
})
export class ValidationsComponent implements OnInit {
  @Input() visible: boolean;
  @Input() validations: object[];

  constructor() {}

  ngOnInit(): void {
  }
}
