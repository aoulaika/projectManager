import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { GdprService } from '../../../services/gdpr.service';

@Component({
  selector: 'project-gdpr',
  templateUrl: './gdpr.component.html',
  styleUrls: ['./gdpr.component.scss']
})
export class GdprComponent implements OnInit {
  @Input() project: object;
  id: string;
  gdprForm: FormGroup;
  gdprData: {
    data: {
      isEuropean: boolean,
      sensitivityLevel: string,
      isDataProcessor: boolean,
      clearOrEncrypted: string
    }
    hosting: {
      whoHostData: string,
      hostinglink: boolean,
      askingHostingContract: boolean
    },
    data_management: {
      isAccessingData: boolean,
      howDataAccess: string,
      howWillAccess: string[]
    }
  };
  
  sensitivityLevel: string;
  whoHostData: string;
  isAccessingData: boolean;

  showClearOrEncrypted: boolean;
  showHostLink: boolean;
  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private route: ActivatedRoute,
    private gdprService: GdprService)
  {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit() {

    this.gdprForm = this.fb.group({
        gdprData: this.fb.group({
          data: this.fb.group({
            isEuropean: [false],
            sensitivityLevel: [''],
            isDataProcessor: [false],
            clearOrEncrypted: ['']
          }),
          hosting: this.fb.group({
            whoHostData: [''],
            hostinglink: [false],
            askingHostingContract:[false]
          }),
          data_management: this.fb.group({
            isAccessingData: [false],
            howDataAccess: '',
            howWillAccess: ['']
          })
        })
    });

    this.gdprData = {
      data: {
        isEuropean: false, sensitivityLevel: "", isDataProcessor: false, clearOrEncrypted: ""
      },
      hosting: {whoHostData: "", hostinglink: false, askingHostingContract: false},
      data_management: { isAccessingData: false, howDataAccess: "", howWillAccess: [] }
    };

    this.getProjectGdpr();
  }

  createProjectGdpr() {
    this.gdprData = { ...this.gdprForm.value.gdprData};
    const api_url = `/api/projects/${this.id}/gdpr`;

    this.gdprService.createProjectGdpr(api_url, this.gdprData)
      .toPromise()
      .then(data => {
        console.log('data', data);
      })
      .catch(err => console.log(err));
  }

  getProjectGdpr() {
    const api_url = `/api/projects/${this.id}/gdpr`;

    this.gdprService.getProjectGdpr(api_url)
      .toPromise()
      .then(data => {
        this.showClearOrEncrypted = (data['data'] || {}).isDataProcessor || false;
        this.showHostLink = (data['hosting'] || {}).hostinglink || false;

        this.gdprForm.patchValue({
            gdprData:{
              data: {
                isEuropean: (data['data'] || {}).isEuropean || false,
                sensitivityLevel: (data['data'] || {}).sensitivityLevel || '',
                isDataProcessor: (data['data'] || {}).isDataProcessor || false,
                clearOrEncrypted: (data['data'] || {}).clearOrEncrypted || ''
              },
              hosting: {
                whoHostData: (data['hosting'] || {}).whoHostData || 'unsupported',
                hostinglink: (data['hosting'] || {}).hostinglink || false,
                askingHostingContract: (data['hosting'] || {}).askingHostingContract || false
              },
              data_management: {
                isAccessingData: (data['data_management'] || {}).isAccessingData || false,
                howDataAccess: (data['data_management'] || {}).howDataAccess || 'false',
                howWillAccess: (data['data_management'] || {}).howWillAccess || [''],
              }
            }
          }
        );
      })
      .catch(err => console.log(err));
  }

  OnIsDataProcessorChange(newValue)  {
      console.log("newvalue: ", newValue.checked);
      this.showClearOrEncrypted = newValue.checked;
  }

  OnhostinglinkChange(newValue)  {
    console.log("newvalue: ", newValue.checked);
    this.showHostLink = newValue.checked;
    // "whoHostData=='third party'"
}
}
