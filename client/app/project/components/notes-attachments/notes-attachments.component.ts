import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ProjectService } from "../../../services/project.service";
import { NgProgress } from "ngx-progressbar";

@Component({
  selector: "project-notes-attachments",
  templateUrl: "./notes-attachments.component.html",
  styleUrls: ["./notes-attachments.component.scss"]
})
export class NotesAttachmentsComponent implements OnInit {
  @Input() id: string;
  @Input() project: object;
  @Input() notes: object[];
  @Input() attachments: object[];
  notesForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private pService: NgProgress,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.notesForm = this.fb.group({
      note: [""]
    });
  }

  // Project notes management
  addNote() {
    const { value } = this.notesForm;
    this.notesForm.reset();
    if (value.note !== "") {
      const note = {
        note: value.note
      };
      this.pService.start();
      this.projectService.addProjectNote(this.id, note).subscribe(
        data => {
          this.project["notes"].unshift(data);
          this.notesForm.reset();
        },
        error => console.log(error),
        () => this.pService.done()
      );
    }
  }

  removeNote(id) {
    this.pService.start();
    this.projectService
      .removeProjectNote({ project_id: this.id, note_id: id })
      .subscribe(
        data => {
          this.project["notes"] = this.project["notes"].filter(
            note => note._id !== id
          );
        },
        error => console.log(error),
        () => this.pService.done()
      );
  }

  fileChange(event) {
    const element = event.srcElement;
    const files = element.files;
    const fd = new FormData();
    for (let i = 0; i < files.length; i++) {
      fd.append("attachments", files[i], files[i].name);
    }
    this.pService.start();
    this.projectService.upload(this.id, fd).subscribe(
      data => {
        this.project["attachments"].unshift(...data);
        element.value = "";
        this.pService.done();
      },
      error => {
        console.log(error);
        element.value = "";
        this.pService.done();
      }
    );
  }

  removeFile(item) {
    this.projectService.removeProjectAttachment(this.id, item).subscribe(
      data => {
        this.project["attachments"].splice(item, 1);
      },
      error => console.log(error)
    );
  }

  downloadFile(item) {
    this.projectService.downloadProjectAttachment(this.id, item);
  }
}
