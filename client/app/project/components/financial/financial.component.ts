import { Component, Input, OnInit } from "@angular/core";
import { ProfileService } from "../../../services/profile.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ProjectService } from "../../../services/project.service";

@Component({
  selector: "project-financial",
  templateUrl: "./financial.component.html",
  styleUrls: ["./financial.component.scss"]
})
export class FinancialComponent implements OnInit {
  @Input() visible: boolean;
  @Input() project: {};
  @Input() id: string;
  profilesBudget: object[] = [];
  discountForm: FormGroup;
  editDiscount = false;

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {
    //this.getProfiles();
    this.getProjectProfiles();
    this.discountForm = this.fb.group({
      discount: [
        this.project["discount"] || 0,
        [Validators.required, Validators.min(0), Validators.max(100)]
      ]
    });
  }

  private getProjectProfiles() {
    this.profileService.getProjectProfiles(this.id).subscribe(
      data => {
        this.profilesBudget = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  private getProfiles() {
    this.profileService.getProfiles().subscribe(
      data => {
        this.profilesBudget = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  getSprintFeatures(sprintId) {
    const features = this.project["features"]
      .filter(feature => feature.sprint === sprintId)
      .map(feature => feature.feature);
    if (features.length > 0) {
      return features;
    }
    return "No feature linked to sprint";
  }

  updateDiscount() {
    const { value: project } = this.discountForm;
    project["_id"] = this.id;
    this.projectService.editProject(project).subscribe(
      data => {
        this.project["discount"] = project["discount"];
        this.editDiscount = !this.editDiscount;
      },
      err => {
        console.log(err);
        this.editDiscount = !this.editDiscount;
      }
    );
  }

  get subTotal(): number {
    if (!this.project["profiles"]) {
      return 0;
    }
    return this.profilesBudget.reduce(
      (acc: number, value: { budget: number; label: string }) => {
        return acc + value.budget * this.project["profiles"][value.label];
      },
      0
    ) as number;
  }

  get totalDiscount(): number {
    if (!this.project["profiles"]) {
      return 0;
    }
    return this.subTotal * (this.project["discount"] || 0) / 100;
  }

  get totalHT(): number {
    if (!this.project["profiles"]) {
      return 0;
    }
    return this.subTotal - this.totalDiscount;
  }

  get totalVAT(): number {
    if (!this.project["profiles"]) {
      return 0;
    }
    return this.subTotal * (this.project["customer"].vat || 0) / 100;
  }

  get totalTTC(): number {
    if (!this.project["profiles"]) {
      return 0;
    }
    return +(this.totalHT + this.totalVAT).toFixed(2);
  }
}
