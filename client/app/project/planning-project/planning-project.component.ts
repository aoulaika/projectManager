import {Component, OnInit, ViewChild} from '@angular/core';


import {ProjectService} from '../../services/project.service';
import {ResourceService} from '../../services/resource.service';
import { AuthService } from '../../services/auth.service';

@Component({
 selector: 'app-planning-project',
 templateUrl: './planning-project.component.html',
 styleUrls: ['./planning-project.component.scss']
})
export class PlanningProjectComponent implements OnInit {
  events: any[];
  projectsPlanning: any[];
  resources: any[];
  headerConfig: any;
  isLoading = true;
  displayDialog = false;
  project = {};
  index = 0;
  isSubsidiary = false;
  search: string = "";

  constructor(private projectService: ProjectService, private resourceService: ResourceService, private auth: AuthService) {
    this.isSubsidiary = auth.currentUser.role === "subsidiary";
  }

  ngOnInit() {
    this.headerConfig = {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    };
    this.getProjectsPlanning();
    this.getResources();
  }

  getProjectsPlanning() {
    this.projectService.getProjectsPlanning()
      .subscribe(
        data => {
          this.events = this.projectsPlanning = data.map(p => {
            p.selected = false;
            return p;
          });
          this.isLoading = false;
        },
        error => {
          console.log(error);
          this.isLoading = false;
        }
      );
  }

  getResources() {
    this.resourceService.getResources()
      .subscribe(
        data => this.resources = data.map(resource => {
          resource.selected = false;
          return resource;
        }),
        err => console.log(err)
      );
  }

  handleEventClick(e) {
    const project_id = e.calEvent.id;
    if (project_id && project_id !== '') {
      this.projectService.getProject({_id: project_id})
        .subscribe(
          data => {
            this.displayDialog = true;
            this.project = data;
          }, error => {
            console.log(error);
          }
        );
    }
  }

  getPlanning() {
    let selected = [];
    if (this.index === 0) {
      selected = this.projectsPlanning.filter(project => project.selected).map(project => project.id);
      if (selected.length > 0) {
        this.projectService.getProjectPlanning({data: selected})
          .subscribe(
            data => {
              this.events = data;
            },
            err => console.log(err)
          );
      }
    } else if (this.index === 1) {
      selected = this.resources.filter(resource => resource.selected).map(resource => resource._id);
      if (selected.length > 0) {
        this.resourceService.getResourcesPlanning({data: selected})
          .subscribe(
            data => {
              this.events = data;
            },
            err => console.log(err)
          );
      }
    }
    if (selected.length === 0) {
      this.events = this.projectsPlanning;
    }
  }

  clearSelection() {
    this.projectsPlanning.map(project => {
      project.selected = false;
      return project;
    });
    this.resources.map(resource => {
      resource.selected = false;
      return this.resources;
    });
  }

  onTabOpen(index) {
    this.index = index === this.index ? undefined : index;
  }
}
