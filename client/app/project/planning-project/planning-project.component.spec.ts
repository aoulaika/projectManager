import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningProjectComponent } from './planning-project.component';

describe('PlanningProjectComponent', () => {
  let component: PlanningProjectComponent;
  let fixture: ComponentFixture<PlanningProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
