import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProjectService} from '../services/project.service';
import { WebSocketService } from '../services/web-socket.service';
import { Subscription } from 'rxjs/Subscription';

import {
  CompleterData,
  CompleterService,
  RemoteData,
  LocalData,
  CompleterItem
} from "ng2-completer";

@Component({
             selector: 'app-project',
             templateUrl: './project.component.html',
             styleUrls: ['./project.component.scss']
           })
export class ProjectComponent implements OnInit, OnDestroy {
  statusFilter: LocalData;
  projects: any[] = [];
  projectsToShow: any[] = [];
  filterStatus: string[] = [];
  checkboxes: {id, klass, label}[];
  isLoading = true;
  step: string[] = ['service', 'subsidiary', 'manager'];
  projectCreationSubscription: Subscription;

  constructor(private projectService: ProjectService, private webSocketService: WebSocketService, private completerService: CompleterService) {
    this.projectCreationSubscription = this.webSocketService
      .consumeEventOnProjectCreated()
      .subscribe(response => {
        this.getProjects();
      });
  }

  ngOnInit() {
    this.checkboxes = [
      {'id': 'all-level', 'klass': 'checkbox-custom checkbox-info', 'label': 'All'},
      {'id': 'open-level', 'klass': 'checkbox-custom checkbox-info', 'label': 'New'},
      {'id': 'budgeted-level', 'klass': 'checkbox-custom checkbox-primary', 'label': 'Budgeted'},
      {'id': 'onhold-level', 'klass': 'checkbox-custom checkbox-warning', 'label': 'On Hold'},
      {'id': 'completed-level', 'klass': 'checkbox-custom checkbox-info ', 'label': 'Completed'},
      {'id': 'inprogress-level', 'klass': 'checkbox-custom checkbox-primary', 'label': 'In Progress'},
      {'id': 'canceled-level', 'klass': 'checkbox-custom checkbox-warning', 'label': 'Canceled'},
      {'id': 'manager-validation-level', 'klass': 'checkbox-custom checkbox-info', 'label': 'Manager Validation'},
      {'id': 'service-validation-level', 'klass': 'checkbox-custom checkbox-primary', 'label': 'Service Validation'}
    ];

    this.statusFilter = this.completerService.local(
      this.checkboxes,
      "label",
      "label"
    );

    this.getProjects();
  }

  getProjects() {
    this.isLoading = true;
    this.projectService.getProjects().subscribe(
      data => {
        this.projectsToShow = this.projects = data;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  checkStatus(checkbox) {
    let status = checkbox.label.toLowerCase();
    if(status !== "manager validation" && status !== "service validation") {
      status = status.replace(' ', '-');
    }
    
    if (this.filterStatus.includes(status)) {
      this.filterStatus.splice(this.filterStatus.indexOf(status), 1);
    } else {
      this.filterStatus.push(status);
    }
    this.reloadProjects();
  }

  checkStatus2(checkbox) {
    let status = checkbox.title.toLowerCase();
    if(status !== "manager validation" && status !== "service validation") {
      status = status.replace(' ', '-');
    }
    
    this.filterStatus = [];
    this.filterStatus.push(status);
    
    this.reloadProjects();
  }

  reloadProjects() {
    if (this.filterStatus[0] === 'all' || !this.filterStatus.length || this.filterStatus.length === this.checkboxes.length) {
      this.projectsToShow = this.projects;
    } else {
      this.projectsToShow = this.projects.filter(item => this.filterStatus.includes(item.status[0].label));
    }
  }

  ngOnDestroy(): void {
    this.projectCreationSubscription.unsubscribe();
  }

}
