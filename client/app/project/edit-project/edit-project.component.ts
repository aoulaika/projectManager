import {
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import {
  CompleterData,
  CompleterItem,
  CompleterService,
  RemoteData,
  LocalData
} from "ng2-completer";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgProgress } from "ngx-progressbar";
import { Subject } from "rxjs/Subject";
import * as moment from "moment";

import { ResourceService } from "../../services/resource.service";
import { ProjectService } from "../../services/project.service";
import { SprintService } from "../../services/sprint.service";
import { WebSocketService } from "../../services/web-socket.service";
import { OnDestroy } from "@angular/core/src/metadata/lifecycle_hooks";
import { Subscription } from "rxjs/Subscription";
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-edit-project",
  templateUrl: "./edit-project.component.html",
  styleUrls: ["./edit-project.component.scss"]
})
export class EditProjectComponent implements OnInit, OnDestroy {
  id: string;
  ownerUsers: LocalData;
  serviceUsers: LocalData;
  managerUsers: LocalData;
  customerData: RemoteData;
  @ViewChild("featureName") featureName;
  @ViewChild("featureDesc") featureDesc;
  @ViewChild("featureSprint") featureSprint;
  @ViewChild("featureBudget") featureBudget;
  @ViewChild("sprintStartDate") sprintStartDate;
  @ViewChild("sprintEndDate") sprintEndDate;
  EditProjectForm: FormGroup;
  featureForm: FormGroup;
  sprintForm: FormGroup;
  profilesForm: FormGroup;
  isLoading = true;
  isEditing = false;
  isEditingFeature = "";
  isEditingSprint = "";
  canBudgetProject = true;
  project = {};
  resources: CompleterData;
  featureBudgetSum = 0;
  profileBudgetSum = 0;
  typeValues = [
    { name: "4D", selected: false, id: 1 },
    { name: "4D Mobile", selected: false, id: 2 },
    { name: "Mobile", selected: false, id: 3 },
    { name: "Wakanda", selected: false, id: 4 },
    { name: "Other", selected: false, id: 5 }
  ];
  profiles: String[] = [
    "architect",
    "mobile",
    "backend",
    "web",
    "designer",
    "tester",
    "manager"
  ];
  parentToChild: Subscription;
  private resourceSubject = new Subject();
  canReject = true;
  stakeholders = { subsidiary: "", service: "", manager: "" };

  constructor(
    private formBuilder: FormBuilder,
    private pService: NgProgress,
    private auth: AuthService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private sprintService: SprintService,
    private resourceService: ResourceService,
    private completerService: CompleterService,
    private webSocketService: WebSocketService,
    private userService: UserService
  ) {
    this.canReject = auth.currentUser.role != "subsidiary";
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getProject();
    });
    this.parentToChild = this.projectService.parentToChild.subscribe(
      (status: string) => {
        this.updateProjectStatus(status);
      }
    );
    this.customerData = completerService.remote(null, "name", "name");
    this.customerData.urlFormater(term => {
      return `/api/customers?query=${term}`;
    });
    this.customerData.dataField("customers");
  }

  ngOnInit() {
    this.EditProjectForm = this.formBuilder.group({
      name: [{ value: "", disabled: true }, Validators.required],
      createdAt: [""],
      createdBy: [""],
      description: [{ value: "", disabled: true }],
      customer: [{ value: "", disabled: true }, Validators.required],
      language: [{ value: "", disabled: true }],
      technologies: [{ value: "", disabled: true }],
      status: [{ value: "", disabled: true }],
      types: [{ value: "", disabled: true }, Validators.required],
      startDate: [{ value: "", disabled: true }, Validators.required],
      stakeholders: this.formBuilder.group({
        subsidiary: [{value: "", disabled:true}],
        service: [{value: "", disabled:true}],
        manager: [{value: "", disabled:true}]
      })
    });
    this.featureForm = this.formBuilder.group({
      feature: [""],
      description: [""],
      sprint: [""],
      budget: [0]
    });
    this.sprintForm = this.formBuilder.group({
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      duration: [0]
    });
    this.profilesForm = this.formBuilder.group({
      architect: [0],
      mobile: [0],
      backend: [0],
      web: [0],
      designer: [0],
      tester: [0],
      manager: [0]
    });
    this.getStakeholders();
  }

  getProject() {
    this.pService.start();
    this.projectService.getProject({ _id: this.id }).subscribe(
      data => {
        this.projectService.notifyParent(data["status"].slice(-1).pop().label);
        this.project = data;
        this.EditProjectForm = this.formBuilder.group({
          name: [
            { value: data["name"], disabled: true },
            Validators.required
          ],
          createdAt: [data["createdAt"]],
          createdBy: [
            `${data["createdBy"]["firstName"]} ${
              data["createdBy"]["lastName"]
            }`
          ],
          description: [data["description"]],
          customer: [data["customer"].name, Validators.required],
          language: [data["language"]],
          technologies: [
            { value: data["technologies"], disabled: true }
          ],
          status: [data["status"].slice(-1).pop().label],
          types: this.buildTypes(),
          startDate: [
            { value: data["startDate"], disabled: true },
            Validators.required
          ],
          stakeholders: this.formBuilder.group({
            subsidiary: [{value: data.stakeholders.subsidiary, disabled: true}],
            service: [{value: data.stakeholders.service, disabled: true}],
            manager: [{value: data.stakeholders.manager, disabled: true}]
          })
        });

        this.profilesForm.setValue({
            architect: (data["profiles"] || {}).architect || 0,
            mobile: (data["profiles"] || {}).mobile || 0,
            backend: (data["profiles"] || {}).backend || 0,
            web: (data["profiles"] || {}).web || 0,
            designer: (data["profiles"] || {}).designer || 0,
            tester: (data["profiles"] || {}).tester || 0,
            manager: (data["profiles"] || {}).manager || 0
        });

        if (data["validations"].length > 0) {
          const lastProcess = data["validations"].slice(-1).pop();
          const test = lastProcess.history.find(h => !h.validation);
          this.canBudgetProject = !!test;
        }
        this.getResources();
        this.updateFeatureBudgetSum();
        this.updateProfileBudgetSum();
      },
      error => console.log(error),
      () => {
        this.isLoading = false;
        this.pService.done();
      }
    );
  }

  getResources(update = false) {
    this.resourceService.getResources().subscribe(
      data => {
        const p_r = this.project["resources"].map(r => r.resource["_id"]);
        const r = data.filter(resource => p_r.indexOf(resource._id) === -1);
        if (!update) {
          this.resources = this.completerService.local(
            this.resourceSubject,
            "name",
            "name"
          );
        }
        this.resourceSubject.next(r);
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  // Project resources management
  addResource(selected: CompleterItem) {
    if (selected) {
      const resource = {
        resource: selected.originalObject._id
      };
      this.pService.start();
      this.projectService.addProjectResource(this.id, resource).subscribe(
        data => {
          this.project["resources"].unshift({
            resource: selected.originalObject
          });
          this.getResources(true);
          this.pService.done();
        },
        error => {
          console.log(error);
          this.pService.done();
        }
      );
    }
  }

  removeResource(resource_id) {
    this.pService.start();
    this.projectService.removeProjectResource(this.id, resource_id).subscribe(
      data => {
        this.project["resources"] = this.project["resources"].filter(
          elt => elt.resource._id !== resource_id
        );
        this.getResources(true);
      },
      error => {
        console.log(error);
        this.pService.done();
      }
    );
  }

  private buildTypes() {
    const arr = this.typeValues.map(type => {
      if (this.project["types"].indexOf(type.name) !== -1) {
        return this.formBuilder.control({ value: true, disabled: true });
      } else {
        return this.formBuilder.control({
          value: type.selected,
          disabled: true
        });
      }
    });
    return this.formBuilder.array(arr);
  }

  // Project features management
  addFeature() {
    const {value: feature} = this.featureForm;
    if (feature.feature && feature.budget) {
      if (!feature.sprint) {
        delete feature.sprint;
      }
      this.projectService.addProjectFeature(this.id, feature).subscribe(
        data => {
          if (data.hasOwnProperty("budgeted") && data.budgeted) {
            this.webSocketService.emitEventOnProjectBudgeted(
              this.project["name"]
            );
            delete data.budgeted;
          }
          this.project["features"].push(data);
          this.featureForm.reset();
          this.updateFeatureBudgetSum();
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  editFeature(feature) {
    const shouldUpdate = feature._id === this.isEditingFeature;
    if (shouldUpdate) {
      const newFeature = {
        feature: this.featureName.nativeElement.value,
        description: this.featureDesc.nativeElement.value,
        sprint: this.featureSprint.nativeElement.value,
        budget: +this.featureBudget.nativeElement.value
      };
      this.pService.start();
      this.projectService
        .updateProjectFeature(this.id, feature._id, newFeature)
        .subscribe(
          data => {
            this.project["features"] = data;
            this.updateFeatureBudgetSum();
            this.isEditingFeature = "";
            this.pService.done();
          },
          error => {
            console.log(error);
            this.isEditingFeature = "";
            this.pService.done();
          }
        );
    }
    this.isEditingFeature = feature._id;
  }

  removeFeature(id) {
    this.pService.start();
    this.projectService
      .removeProjectFeature({ project_id: this.id, feature_id: id })
      .subscribe(
        data => {
          this.project["features"] = this.project["features"].filter(
            feature => feature._id !== id
          );
          this.updateFeatureBudgetSum();
          this.pService.done();
        },
        error => {
          console.log(error);
          this.pService.done();
        }
      );
  }

  editProjectDetail() {
    this.EditProjectForm.controls["name"].enable();
    this.EditProjectForm.controls["customer"].enable();
    this.EditProjectForm.controls["description"].enable();
    this.EditProjectForm.controls["language"].enable();
    this.EditProjectForm.controls["stakeholders"].enable();
    if (!this.project["startDate"]) {
      this.EditProjectForm.controls["startDate"].enable();
    }
    this.types.enable();
    this.isEditing = true;
  }

  saveEditProjectDetail() {
    const {value: project} = this.EditProjectForm;
    project["_id"] = this.id;
    const t = this.typeValues
      .filter((item, index) => project.types[index])
      .map(item => item.name);
    delete project["status"];
    delete project["createdAt"];
    delete project["createdBy"];
    project.types = t;
    Object.entries(this.stakeholders).forEach(([attribut, value]) => {
      if (value) {
        project.stakeholders[attribut] = value;
      } else {
        delete project.stakeholders[attribut];
      }
    });
    if (Object.keys(project.stakeholders).length === 0) {
      delete project["stakeholders"];
    }
    this.pService.start();
    this.projectService.editProject(project).subscribe(
      data => {
        this.isEditing = false;
        this.disableFormDetail();
        this.pService.done();
      },
      error => {
        this.resetFormValue();
        this.disableFormDetail();
        this.isEditing = false;
        this.pService.done();
      }
    );
  }

  updateSkateholders(data: { field: string; event: CompleterItem }) {
    this.stakeholders[data.field] = data.event.originalObject["_id"];
  }

  cancelEditProjectDetail() {
    this.disableFormDetail();
    this.resetFormValue();
  }

  setProfiles() {
    const {value: profiles} = this.profilesForm;
    this.pService.start();
    this.projectService.addProjectProfiles(this.id, profiles).subscribe(
      data => {
        if ("budgeted" in data && data["budgeted"]) {
          this.webSocketService.emitEventOnProjectBudgeted(
            this.project["name"]
          );
          delete data["budgeted"];
        }
        this.project["profiles"] = data["profiles"];
        this.pService.done();
      },
      error => {
        console.log(error);
        this.pService.done();
      }
    );
  }

  updateProfiles(targetName: string = "") {
    const {value: profiles} = this.profilesForm;
    const sum =
      profiles.architect + profiles.mobile + profiles.web + profiles.backend;
    if (!targetName) {
      const designer = Math.round(+(sum * 0.1).toFixed(2));
      this.profilesForm
        .get("designer")
        .setValue(designer);
      const tester = Math.round(+(0.2 * 1.1 * sum).toFixed(2));
      this.profilesForm.get("tester").setValue(tester);
      const manager = Math.round(+(0.3 * 1.1 * 1.2 * sum).toFixed(2));
      this.profilesForm
        .get("manager")
        .setValue(manager);
      this.profileBudgetSum = +(sum + designer + tester + manager).toFixed(2);
    } else {
      if (targetName === "designer") {
        const tester = Math.round(
          +(0.2 * (profiles.designer + sum)).toFixed(2)
        );
        this.profilesForm
          .get("tester")
          .setValue(tester);
        const manager = Math.round(
          +(0.3 * (profiles.designer + tester + sum)).toFixed(2)
        );
        this.profilesForm
          .get("manager")
          .setValue(manager);
        this.profileBudgetSum = +(
          sum +
          profiles.designer +
          tester +
          manager
        ).toFixed(2);
      } else if (targetName === "tester") {
        const manager = Math.round(
          +(0.3 * (profiles.designer + profiles.tester + sum)).toFixed(2)
        );
        this.profilesForm
          .get("manager")
          .setValue(manager);
        this.profileBudgetSum = +(
          sum +
          profiles.designer +
          profiles.tester +
          manager
        ).toFixed(2);
      } else {
        this.profileBudgetSum = +(
          sum +
          profiles.designer +
          profiles.tester +
          profiles.manager
        ).toFixed(2);
      }
    }
  }

  resetFormValue() {
    this.EditProjectForm.get("name").setValue(this.project["name"]);
    this.EditProjectForm.get("customer").setValue(
      this.project["customer"]["name"]
    );
    this.EditProjectForm.get("description").setValue(
      this.project["description"]
    );
    const n = this.types.length;
    const arr = this.buildTypes();
    for (let i = 0; i < n; i++) {
      this.types.setControl(i, arr.at(i));
    }
  }

  disableFormDetail() {
    this.EditProjectForm.controls["name"].disable();
    this.EditProjectForm.controls["customer"].disable();
    this.EditProjectForm.controls["description"].disable();
    this.EditProjectForm.controls["startDate"].disable();
    this.EditProjectForm.controls["language"].disable();
    this.EditProjectForm.controls["stakeholders"].disable();
    this.types.disable();
    this.isEditing = false;
  }

  downloadFile(item) {
    this.projectService.downloadProjectAttachment(this.id, item);
  }

  get types() {
    return this.EditProjectForm.get("types") as FormArray;
  }

  updateFeatureBudgetSum() {
    if (this.project["features"]) {
      this.featureBudgetSum = this.project["features"].reduce((sum, f) => {
        return sum + Number(f.budget);
      }, 0);
    }
  }

  updateProfileBudgetSum() {
    if (this.project["profiles"]) {
      const profiles = this.project["profiles"];
      this.profileBudgetSum = Object.values(profiles).reduce(
        (sum: number, value) => {
          return sum + Number(value);
        },
        0
      ) as number;
    }
  }

  validateProject(validate, comment) {
    const data = {
      validation: validate,
      comment: comment
    };
    this.pService.start();
    this.projectService.addProjectValidation(this.id, data).subscribe(
      response => {
        const validations = this.project["validations"];
        const isValid =
          validations.length === 0
            ? false
            : validations
                .slice(-1)
                .pop()
                .history.filter(history => history.validation).length === 2;
        if (data.validation && isValid) {
          this.webSocketService.emitEventOnZohoProjectCreated(
            this.project["name"]
          );
        }
        this.getProject();
        this.webSocketService.emitEventOnProjectValidated(this.project["name"]);
      },
      error => {
        this.pService.done();
      },
      () => this.pService.done()
    );
  }

  updateResourceProfile(id, profile) {
    this.pService.start();
    this.projectService
      .updateResourceProfile(this.id, id, { role: profile })
      .subscribe(
        data => {
          console.log(data);
          this.pService.done();
        },
        err => this.pService.done()
      );
  }

  addSprint() {
    const {value: sprint} = this.sprintForm;
    this.sprintService.createSprint(this.id, sprint).subscribe(
      data => {
        if ("budgeted" in data && data.budgeted) {
          this.webSocketService.emitEventOnProjectBudgeted(
            this.project["name"]
          );
          delete data.budgeted;
        }
        this.project["sprints"].push(data);
        this.sprintForm.reset();
      },
      err => console.log(err)
    );
  }

  updateDuration() {
    const {value: sprint} = this.sprintForm;
    if (sprint.endDate !== "" && sprint.startDate !== "") {
      let duration =
        moment(sprint.endDate).diff(moment(sprint.startDate), "days") + 1;
      duration -= this.countWeekendDays(
        new Date(sprint.startDate),
        new Date(sprint.endDate)
      );
      this.sprintForm
        .get("duration")
        .setValue(duration);
    }
  }

  editSprint(sprint) {
    const shouldUpdate = sprint._id === this.isEditingSprint;
    if (shouldUpdate) {
      const newSprint = {
        startDate: this.sprintStartDate.nativeElement.value.toString(),
        endDate: this.sprintEndDate.nativeElement.value.toString()
      };
      this.pService.start();
      this.sprintService.updateSprint(this.id, sprint._id, newSprint).subscribe(
        data => {
          this.project["sprints"] = data;
          this.pService.done();
        },
        error => {
          console.log(error);
          this.pService.done();
        }
      );
    }
    this.isEditingSprint = shouldUpdate ? "" : sprint._id;
  }

  updateProjectStatus(status: string) {
    this.pService.start();
    this.projectService.updateProjectStatus(this.id, status).subscribe(
      data => {
        this.projectService.notifyParent(data["status"].slice(-1).pop().label);
        this.project["status"] = data["status"];
        this.EditProjectForm.controls["status"].setValue(
          data["status"].slice(-1).pop().label
        );
      },
      err => {
        console.log(err);
        this.pService.done();
      },
      () => {
        this.pService.done();
      }
    );
  }

  export() {
    this.projectService.exportBudgeting(this.id);
  }

  private countWeekendDays(d0, d1) {
    const ndays =
      1 + Math.round((d1.getTime() - d0.getTime()) / (24 * 3600 * 1000));
    const nsaturdays = Math.floor((d0.getDay() + ndays) / 7);
    return (
      2 * nsaturdays + (d0.getDay() === 0 ? 1 : 0) - (d1.getDay() === 6 ? 1 : 0)
    );
  }

  private getStakeholders() {
    this.userService.getAll({ admin: false }).subscribe(
      data => {
        const ownerData = data.filter(user => user.role == "subsidiary");
        this.ownerUsers = this.completerService.local(
          ownerData,
          "firstName,lastName",
          "firstName,lastName"
        );
        const serviceData = data.filter(user => user.role == "service");
        this.serviceUsers = this.completerService.local(
          serviceData,
          "firstName,lastName",
          "firstName,lastName"
        );
        const managerData = data.filter(user => user.role == "manager");
        this.managerUsers = this.completerService.local(
          managerData,
          "firstName,lastName",
          "firstName,lastName"
        );
      },
      error => {
        console.error(error);
      }
    );
  }

  ngOnDestroy() {
    this.parentToChild.unsubscribe();
  }
}
