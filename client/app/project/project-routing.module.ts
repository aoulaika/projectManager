import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ProjectComponent} from './project.component';
import {NewProjectComponent} from './new-project/new-project.component';
import {EditProjectComponent} from './edit-project/edit-project.component';
import {PlanningProjectComponent} from './planning-project/planning-project.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        data: {
          title: 'Projects list'
        },
        component: ProjectComponent
      },
      {
        path: 'planning',
        data: {
          title: 'Projects Planning'
        },
        component: PlanningProjectComponent
      },
      {
        path: 'new',
        data: {
          title: 'New project'
        },
        component: NewProjectComponent
      },
      {
        path: ':id',
        data: {
          title: 'Project details'
        },
        component: EditProjectComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {
}
