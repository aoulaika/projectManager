import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2CompleterModule} from 'ng2-completer';
import {NgProgressModule} from 'ngx-progressbar';
import {AccordionModule, DialogModule, ScheduleModule, TooltipModule} from 'primeng/primeng';

import {ProjectRoutingModule} from './project-routing.module';
import {ProjectComponent} from './project.component';
import {NewProjectComponent} from './new-project/new-project.component';
import {EditProjectComponent} from './edit-project/edit-project.component';
import {PlanningProjectComponent} from './planning-project/planning-project.component';
import {LoadingComponent} from '../shared/loading/loading.component';
import {SprintService} from '../services/sprint.service';
import {SprintIndexPipe} from '../shared/sprint-index.pipe';
import {FilterPipe} from '../shared/filter.pipe';
import { FinancialComponent } from './components/financial/financial.component';
import { ValidationsComponent } from './components/validations/validations.component';
import { NotesAttachmentsComponent } from './components/notes-attachments/notes-attachments.component';
import { GdprComponent } from './components/gdpr/gdpr.component';

@NgModule({
  imports: [
    ProjectRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2CompleterModule,
    ScheduleModule,
    DialogModule,
    AccordionModule,
    TooltipModule,
    NgProgressModule
  ],
  declarations: [
    ProjectComponent,
    NewProjectComponent,
    EditProjectComponent,
    PlanningProjectComponent,
    LoadingComponent,
    SprintIndexPipe,
    FilterPipe,
    FinancialComponent,
    ValidationsComponent,
    NotesAttachmentsComponent,
    GdprComponent
  ],
  providers: [
    SprintService
  ]
})
export class ProjectModule {
}
