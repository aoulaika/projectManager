import {BrowserModule} from '@angular/platform-browser';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {CookieService} from 'ngx-cookie-service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';



import {RoutingModule} from './routing.module';
import {SharedModule} from './shared/shared.module';
import {UserService} from './services/user.service';
import {AuthService} from './services/auth.service';
import {AuthGuardLogin} from './services/auth-guard-login.service';
import {AuthGuardAdmin} from './services/auth-guard-admin.service';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {FullLayoutComponent} from './full-layout/full-layout.component';
import {ProjectService} from './services/project.service';
import {CustomerService} from './services/customer.service';
import {UnauthorizedService} from './shared/unauthorized.service';
import {ResourceService} from './services/resource.service';
import {WebSocketService} from './services/web-socket.service';
import {ToastComponent} from './shared/toast/toast.component';
import {ProfileComponent} from './profile/profile.component';
import {CustomersComponent} from './customers/customers.component';
import {UsersComponent} from './users/users.component';
import {SubsidiaryComponent} from './subsidiary/subsidiary.component';
import {SubsidiaryService} from './services/subsidiary.service';
import {ProfilesComponent} from './profiles/profiles.component';
import {ProfileService} from './services/profile.service';
import {NotificationService} from './services/notification.service';
import { CustomerDetailsComponent } from './customers/customer-details/customer-details.component';
import { PagerService } from './services/page.service';
import { GdprService } from './services/gdpr.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    UsersComponent,
    SubsidiaryComponent,
    NotFoundComponent,
    FullLayoutComponent,
    ToastComponent,
    CustomersComponent,
    CustomerDetailsComponent,
    ProfilesComponent
  ],
  imports: [
    HttpClientModule,
    RoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    BrowserModule,
    ToasterModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: UnauthorizedService, multi: true},
    AuthService,
    AuthGuardLogin,
    AuthGuardAdmin,
    UserService,
    ProjectService,
    CustomerService,
    ResourceService,
    SubsidiaryService,
    CookieService,
    WebSocketService,
    ToasterService,
    ToastComponent,
    ProfileService,
    NotificationService,
    PagerService,
    GdprService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule {

  constructor() {
    // Consume user events
    // this.webSocketService.consumeEventOnWelcomeUserLoggedIn();
    // this.webSocketService.consumeEventOnNotifyUserLoggedIn();

  }

}
