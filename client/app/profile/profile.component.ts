import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {SubsidiaryService} from '../services/subsidiary.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  passwordForm: FormGroup;
  profile: {};
  subsidiaries: {}[];
  roles: string[] = ["admin", "service", "subsidiary", "manager"];

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private subsidiaryService: SubsidiaryService,
    public toast: ToastComponent
  ) {}

  ngOnInit() {
    const controlConfig = {
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      phone: ["", Validators.required],
      email: ["", Validators.required],
      subsidiary: ["", Validators.required],
      avatar: [""]
    };
    this.profileForm = this.formBuilder.group(controlConfig);
    const passwordControlConfig = {
      oldPassword: ["", Validators.required],
      newPassword: ["", Validators.required],
      confirmPassword: ["", Validators.required]
    };
    this.passwordForm = this.formBuilder.group(passwordControlConfig, {
      validator: this.MatchPassword
    });
    this.getProfile();
    this.getSubsidiaries();
  }

  updateProfile() {
    const data = this.profileForm.value;
    this.auth.updateProfile(data).subscribe(
      res => {
        this.toast.setMessage('profile updated succesfully', 'success');
      },
      err => {},
      () => {}
    );
  }

  updatePassword() {
    const data = {
      password: this.passwordForm.value.oldPassword,
      newPassword: this.passwordForm.value.newPassword
    };
    this.auth.changePassword(data).subscribe(
      res => {
        this.toast.setMessage('password updated succesfully', 'success');
      },
      err => {},
      () => {
        this.passwordForm.reset();
      }
    );
  }

  private MatchPassword(AC: AbstractControl) {
    if (AC.get("newPassword").value !== AC.get("confirmPassword").value) {
      AC.get("confirmPassword").setErrors({ MatchPassword: true });
    } else {
      AC.get("confirmPassword").setErrors(null);
    }
  }

  private getProfile() {
    this.auth.getProfile().subscribe(
      res => {
        this.profileForm.get("firstName").setValue(res.firstName);
        this.profileForm.get("lastName").setValue(res.lastName);
        this.profileForm.get("phone").setValue(res.phone);
        this.profileForm.get("email").setValue(res.email);
        this.profileForm.get("subsidiary").setValue(res.subsidiary);
      },
      err => {},
      () => {
        this.passwordForm.reset();
      }
    );
  }

  private getSubsidiaries() {
    this.subsidiaryService.getSubsidiaries().subscribe(
      res => {
        this.subsidiaries = res;
      },
      err => {},
      () => {}
    );
  }
}
