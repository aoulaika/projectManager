import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {SideBarComponent} from './side-bar/side-bar.component';
import { FilterPipe } from './filter.pipe';

@NgModule({
  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  exports: [
    // Shared Modules
    BrowserModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpModule,
    // Shared Components
    SideBarComponent,
    NavBarComponent,
    BreadcrumbComponent,
  ],
  declarations: [
    NavBarComponent,
    SideBarComponent,
    BreadcrumbComponent,
  ]
})
export class SharedModule {
}
