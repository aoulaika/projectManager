import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
        name: 'sprintIndex'
      })
export class SprintIndexPipe implements PipeTransform {

  transform(id: string, sprints: { _id }[]): any {
    const index = sprints.findIndex(sprint => sprint._id.toString() === id);
    if (index !== -1) {
      return index;
    }
    return null;
  }

}
