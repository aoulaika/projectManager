import {Component} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
             selector: 'app-breadcrumb',
             templateUrl: './breadcrumb.component.html',
             styleUrls: ['./breadcrumb.component.scss']
           })
export class BreadcrumbComponent {
  breadcrumbs: Array<Object>;

  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
      this.breadcrumbs = [];
      let currentRoute = this.route.root,
        url = '';
      do {
        const childrenRoutes = currentRoute.children;
        currentRoute = null;
        childrenRoutes.forEach(rout => {
          if (rout.outlet === 'primary') {
            const routeSnapshot = rout.snapshot;
            url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');
            this.breadcrumbs.push(
              {
                label: rout.snapshot.data,
                url: url
              }
            );
            currentRoute = rout;
          }
        });
      } while (currentRoute);
    });
  }
}
