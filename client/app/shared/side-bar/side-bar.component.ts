import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';


import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent {
  routes: { title: string, route: string, icon: string }[];
  adminRoutes: { title: string, route: string, icon: string }[] = [
    {
      title: 'Pricing',
      route: '/pricing',
      icon: 'fa-eur'
    },
    {
      title: 'Users',
      route: '/users',
      icon: 'fa-users'
    },
    {
      title: 'Subsidiaries',
      route: '/subsidiaries',
      icon: 'fa-briefcase'
    }
  ];
  projectRoutes: { title: string, route: string, icon: string }[] = [
    {
      title: 'My Projects',
      route: '/projects',
      icon: 'fa-list-ul'
    },
    {
      title: 'New Project',
      route: '/projects/new',
      icon: 'fa-plus-square'
    },
    {
      title: 'Planning',
      route: '/projects/planning',
      icon: 'fa-calendar'
    },
    {
      title: 'Customers',
      route: '/customers',
      icon: 'fa-briefcase'
    }
    
  ];

  constructor(private router: Router, private auth: AuthService) {
    this.routes = this.projectRoutes;
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
      this.routes = this.projectRoutes;
      if (auth.currentUser.role === 'admin' && !this.routes.includes(this.adminRoutes[0])) {
        this.routes.push(...this.adminRoutes);
      }
    });
  }

}
