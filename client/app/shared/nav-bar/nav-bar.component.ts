import * as moment from 'moment';

import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
 selector: 'app-nav-bar',
 templateUrl: './nav-bar.component.html',
 styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input('notifications') notifications;
  currentUser: { _id: string, firstName: string, lastName: string, phone: string, role: string, subsidiary: string };

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    this.currentUser = this.auth.currentUser;
  }


  logout() {
    this.auth.logout();
  }

  relativeTime(time) {
    return moment(time).fromNow();
  }
}
