import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  getProfiles(): Observable<any> {
    return this.http.get('/api/profiles');
  }

  // new api used to get profiles budget for the project with the id=pid
  getProjectProfiles(pid): Observable<any> {
    return this.http.get(`/api/projects/${pid}/profiles`, pid);
  }

  updateProfiles(profiles): Observable<any> {
    return this.http.put('/api/profiles', profiles);
  }

}
