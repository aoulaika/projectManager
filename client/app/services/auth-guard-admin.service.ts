import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import { Location } from '@angular/common';

@Injectable()
export class AuthGuardAdmin implements CanActivate {

  constructor(public auth: AuthService, private router: Router, private location: Location) {
  }

  canActivate() {
    if (this.auth.currentUser.role === 'admin') {
      return true;
    }
    this.location.back();
    return false;
  }

}
