import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs/Subject";

@Injectable()
export class ProjectService {
  private headers = new HttpHeaders().set(
    "Content-Type",
    "application/json; charset=utf-8"
  );
  private options = { headers: this.headers };
  parentToChild = new Subject();
  childToParent = new Subject();

  constructor(private http: HttpClient) {}

  getProjects(): Observable<any> {
    return this.http.get("/api/projects", this.options);
  }

  addProject(project): Observable<any> {
    return this.http.post("/api/projects", project);
  }

  getProject(project): Observable<any> {
    return this.http.get(`/api/projects/${project._id}`);
  }

  editProject(project): Observable<any> {
    const {_id, ...data} = project;
    return this.http.put(
      `/api/projects/${_id}`,
      JSON.stringify(data),
      this.options
    );
  }

  updateProjectStatus(projectId, status: string): Observable<any> {
    return this.http.put(`/api/projects/${projectId}/status/${status}`, null);
  }

  // Project notes
  addProjectNote(projectId, note): Observable<any> {
    return this.http.post(
      `/api/projects/${projectId}/notes`,
      JSON.stringify(note),
      this.options
    );
  }

  removeProjectNote(note): Observable<any> {
    return this.http.delete(
      `/api/projects/${note.project_id}/notes/${note.note_id}`,
      this.options
    );
  }

  // Project features
  addProjectFeature(projectId, feature): Observable<any> {
    return this.http.post(
      `/api/projects/${projectId}/features`,
      JSON.stringify(feature),
      this.options
    );
  }

  updateProjectFeature(projectId, featureId, feature): Observable<any> {
    return this.http.put(
      `/api/projects/${projectId}/features/${featureId}`,
      JSON.stringify(feature),
      this.options
    );
  }

  removeProjectFeature(feature): Observable<any> {
    return this.http.delete(
      `/api/projects/${feature.project_id}/features/${feature.feature_id}`,
      this.options
    );
  }

  // Project resources
  addProjectResource(project_id, resource): Observable<any> {
    return this.http.post(
      `/api/projects/${project_id}/resources`,
      JSON.stringify(resource),
      this.options
    );
  }

  removeProjectResource(project_id, resource_id): Observable<any> {
    return this.http.delete(
      `/api/projects/${project_id}/resources/${resource_id}`,
      this.options
    );
  }

  updateResourceProfile(project_id, resource_id, data: { role: String }) {
    return this.http.put(
      `/api/projects/${project_id}/resources/${resource_id}`,
      JSON.stringify(data),
      this.options
    );
  }

  addProjectProfiles(project_id, profiles) {
    return this.http.post(
      `/api/projects/${project_id}/profiles`,
      JSON.stringify(profiles),
      this.options
    );
  }

  upload(project_id, files): Observable<any> {
    return this.http.post(`/api/projects/${project_id}/attachments`, files);
  }

  removeProjectAttachment(project_id, fileName): Observable<any> {
    return this.http.delete(
      `/api/projects/${project_id}/attachments/${fileName}`
    );
  }

  downloadProjectAttachment(project_id, fileName) {
    window.open(
      `/api/projects/${project_id}/attachments/${fileName}`,
      "_blank"
    );
  }

  addProjectValidation(project_id, validation): Observable<any> {
    return this.http.post(
      `/api/projects/${project_id}/validations`,
      JSON.stringify(validation),
      this.options
    );
  }

  getProjectsPlanning(): Observable<any> {
    return this.http.get(`/api/projects/planning`);
  }

  getProjectPlanning(projectsId: { data: string[] }): Observable<any> {
    return this.http.post(
      `/api/projects/planning`,
      JSON.stringify(projectsId),
      this.options
    );
  }

  exportBudgeting(project_id) {
    window.open(`/api/projects/${project_id}/export`, "_blank");
  }

  updateProject(data: string): void {
    this.parentToChild.next(data);
  }

  notifyParent(lastStatus: string): void {
    this.childToParent.next(lastStatus);
  }
}
