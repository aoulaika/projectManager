import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable()
export class UserService {
  private headers = new HttpHeaders().set(
    "Content-Type",
    "application/json; charset=utf-8"
  );
  private options = { headers: this.headers };

  constructor(private http: HttpClient) {}

  register(user): Observable<any> {
    return this.http.post("/api/users", JSON.stringify(user), this.options);
  }

  login(credentials): Observable<any> {
    return this.http.post(
      "/api/login",
      JSON.stringify(credentials),
      this.options
    );
  }

  getAll(data?: {admin?: boolean}): Observable<any> {
    let options = {};    
    if (data) {
      const httpParams = new HttpParams().set("admin", data.admin.toString());
      options = { params: httpParams };
    }
    return this.http.get("/api/users", options);
  }

  changeStatus(user): Observable<any> {
    return this.http.put(`/api/users/${user._id}/changeStatus`, JSON.stringify(user), this.options);
  }
}
