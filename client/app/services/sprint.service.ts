import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SprintService {

  constructor(private http: HttpClient) {
  }

  createSprint(projectId: string, sprint: { name: string }): Observable<any> {
    return this.http.post(`/api/projects/${projectId}/sprints`, sprint);
  }

  updateSprint(projectId: string, sprintId: string, sprint: { startDate: string, endDate: string }): Observable<any> {
    return this.http.put(`/api/projects/${projectId}/sprints/${sprintId}`, sprint);
  }

}
