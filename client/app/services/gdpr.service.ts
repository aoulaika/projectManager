import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GdprService {

  constructor(private http: HttpClient ) {
  }

  createProjectGdpr(api_url: string, gdprData: any): Observable<any> {
    return this.http.post(api_url, gdprData);
  }

  getProjectGdpr(api_url: string): Observable<any> {
    return this.http.get(api_url);
  }
}

