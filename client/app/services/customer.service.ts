import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable()
export class CustomerService {
  private headers = new HttpHeaders().set(
    "Content-Type",
    "application/json; charset=utf-8"
  );
  private options = { headers: this.headers };

  constructor(private http: HttpClient) {}

  getCustomers(page: number = 0, search: string = ""): Observable<any> {
    const httpParams = new HttpParams()
      .set("page", page.toString())
      .set("search", search);
    const options = { params: httpParams };
    return this.http.get("/api/customers", options);
  }

  getCustomer(customerId: string): Observable<any> {
    return this.http.get(`/api/customers/${customerId}`);
  }

  createCustomer(customer): Observable<any> {
    return this.http.post("/api/customers", customer);
  }

  update(
    customerId: string,
    body: { name: string; discounts: number; vat: number }
  ): Observable<any> {
    return this.http.put(
      `/api/customers/${customerId}`,
      JSON.stringify(body),
      this.options
    );
  }

  getCustomerProjects(customerId: string): Observable<any> {
    return this.http.get(`/api/customers/${customerId}/projects`);
  }

  getCustomerContacts(accountId: string): Observable<any> {
    return this.http.get(`/api/customers/${accountId}/contacts`);
  }
}
