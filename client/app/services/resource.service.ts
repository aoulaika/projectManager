import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ResourceService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
  private options = {headers: this.headers};

  constructor(private http: HttpClient) {
  }

  getResources(): Observable<any> {
    return this.http.get('/api/resources');
  }

  getResourcesPlanning(resourcesId: {data: string[]}): Observable<any> {
    return this.http.post(`/api/resources/planning`, JSON.stringify(resourcesId), this.options);
  }
}
