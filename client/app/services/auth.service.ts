import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {JwtHelper} from 'angular2-jwt';

import {UserService} from './user.service';

@Injectable()
export class AuthService {
  loggedIn = false;
  isAdmin = false;

  jwtHelper: JwtHelper = new JwtHelper();

  currentUser: { _id: string, firstName: string, lastName: string, email: string, phone: string, role: string, subsidiary: string };

  constructor(private userService: UserService, private router: Router, private cookieService: CookieService, private http: HttpClient) {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedUser = this.decodeUserFromToken(token);
      this.setCurrentUser(decodedUser);
    }
  }

  login(emailAndPassword) {
    return this.userService.login(emailAndPassword).map(
      res => {
        localStorage.setItem('token', res.token);
        const decodedUser = this.decodeUserFromToken(res.token);
        this.setCurrentUser(decodedUser);
        return this.loggedIn;
      }
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.cookieService.deleteAll();
    this.loggedIn = false;
    this.isAdmin = false;
    this.currentUser = {_id: '', firstName: '', lastName: '', email: '', phone: '', subsidiary: '', role: ''};
    this.router.navigate(['/login']);
  }

  decodeUserFromToken(token) {
    return this.jwtHelper.decodeToken(token).user;
  }

  setCurrentUser(decodedUser) {
    this.loggedIn = true;
    this.currentUser = decodedUser;
    this.isAdmin = this.currentUser.role === 'admin';
  }

  changePassword(data: { password: string, newPassword: string }): Observable<any> {
    return this.http.post('/api/changePassword', data);
  }

  updateProfile(data): Observable<any> {
    return this.http.put('/api/profile', data);
  }

  getProfile(): Observable<any> {
    return this.http.get('/api/profile');
  }

}
