import { Component, OnInit, OnDestroy } from "@angular/core";
import { ToasterConfig, ToasterService } from "angular2-toaster";
import "rxjs/add/operator/merge";

import { NotificationService } from "../services/notification.service";
import { WebSocketService } from "../services/web-socket.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { ProjectService } from "../services/project.service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-full-layout",
  templateUrl: "./full-layout.component.html",
  styleUrls: ["./full-layout.component.scss"]
})
export class FullLayoutComponent implements OnInit, OnDestroy {
  notifications: {}[];
  id: string;
  lastStatus: string;
  isEditProject = false;
  public toastConfig: ToasterConfig = new ToasterConfig({
    positionClass: "toast-top-right"
  });
  webSocketSubscription: Subscription;

  constructor(
    private notificationService: NotificationService,
    private webSocketService: WebSocketService,
    private projectService: ProjectService,
    private router: Router,
    private toasterService: ToasterService
  ) {
    this.projectService.childToParent.subscribe((lastStatus: string) => {
      this.lastStatus = lastStatus;
    });
    const myregexp = /^\/projects\/[0-9a-fA-F]{24}$/;
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((event: NavigationEnd) => {
        const urlAfterRedirects = event.urlAfterRedirects;
        const match = urlAfterRedirects.match(myregexp);
        if (match) {
          this.isEditProject = true;
        } else {
          this.isEditProject = false;
        }
      });
    // Consume project events
    this.webSocketSubscription = this.webSocketService
      .consumeEventOnProjectCreated()
      .merge(
        this.webSocketService.consumeEventOnProjectBudgeted(),
        this.webSocketService.consumeEventOnZohoProjectCreated(),
        this.webSocketService.consumeEventOnWelcomeUserLoggedIn(),
        this.webSocketService.consumeEventOnProjectValidated()
      )
      .subscribe(response => {
        console.log(response);
        if (response.hasOwnProperty("data")) {
          this.notifications.unshift(response.data);
        }
        this.toasterService.pop(response.message);
      });
  }

  ngOnInit() {
    this.getNotifications();
  }

  updateProjectStatus(status: string) {
    if (confirm('Are you sure?')) {
      this.projectService.updateProject(status);
    }
  }

  private getNotifications() {
    this.notificationService.getNotifications().subscribe(
      data => {
        this.notifications = data;
      },
      err => {
        console.log(err);
      },
      () => {
        console.log("done");
      }
    );
  }

  get canValidate(): boolean {
    return (
      this.lastStatus &&
      (this.lastStatus == "new" ||
        this.lastStatus == "budgeted" ||
        this.lastStatus.includes("validation"))
    );
  }

  get canCancel(): boolean {
    return (this.lastStatus !== "canceled");
  }

  get action(): string {
    if (this.lastStatus == "on-hold") return "Resume"
    if (this.lastStatus == "canceled") return ""
  }

  ngOnDestroy() {
    this.webSocketSubscription.unsubscribe();
  }
}
