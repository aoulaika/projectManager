import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SubsidiaryService} from '../services/subsidiary.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: {}[] = [];
  subsidiaries: {}[] = [];
  userForm: FormGroup;
  isLoading = false;
  labels = {
    admin: 'label-danger',
    service: 'label-default',
    subsidiary: 'label-info',
    manager: 'label-warning'
  };
  collapse = false;

  constructor(private userService: UserService, private subsidiaryService: SubsidiaryService, private fb: FormBuilder) {
  }

  ngOnInit() {
    const userFormConfig = {
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      subsidiary: ['', Validators.required],
      role: ['', Validators.required]
    };
    this.userForm = this.fb.group(userFormConfig);
    this.getUsers();
    this.getSubsidiaries();
  }

  private getUsers() {
    this.isLoading = true;
    this.userService.getAll()
      .subscribe(
        data => {
          this.users = data;
        },
        data => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  changeUserStatus(user) {
    user.isActive = !user.isActive;
    this.userService.changeStatus(user)
      .subscribe(
        data => {
          //if(data.success) user.isActive = !user.isActive;
          this.isLoading = false;
        },
        data => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  addUser() {
    this.isLoading = true;
    this.userService.register(this.userForm.value)
      .subscribe(
        data => {
          data.subsidiary = this.subsidiaries.find(subsidiary => subsidiary['_id'] === data.subsidiary);
          this.users.push(data);
          this.userForm.reset();
        },
        data => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  private getSubsidiaries() {
    this.subsidiaryService.getSubsidiaries()
      .subscribe(
        data => {
          this.subsidiaries = data;
        },
        data => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }
}
