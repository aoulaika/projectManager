import { Component, OnInit } from "@angular/core";
import { CustomerService } from "../services/customer.service";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { PagerService } from "../services/page.service";

@Component({
  selector: "app-customers",
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.scss"]
})
export class CustomersComponent implements OnInit {
  customers: {}[];
  customerForm: FormGroup;
  collapse = false;
  currentPage = 1;
  pages = 0;
  pager: any = {};
  search: string = "";

  constructor(
    private customerService: CustomerService,
    private fb: FormBuilder,
    private pagerService: PagerService
  ) {}

  ngOnInit() {
    const customerFormConfig = {
      name: [""],
      vat: [""],
      phone: [""],
      fax: [""],
      discounts: [""]
    };
    this.customerForm = this.fb.group(customerFormConfig);
    this.getCustomers();
  }

  private getCustomers(page: number = 1, search: string = "") {
    this.customerService.getCustomers(page, search).subscribe(
      data => {
        this.customers = data["customers"];
        this.currentPage = data["page"];
        this.pages = data["pages"];
        this.setPager(page);
      },
      error => {},
      () => {}
    );
  }

  saveCustomer() {
    console.log(this.customerForm.value);
    this.customerService.createCustomer(this.customerForm.value).subscribe(
      data => {
        this.customers.push(data);
        this.customerForm.reset();
      },
      error => {
        this.customerForm.reset();
      }
    );
  }

  searchCustomer() {
    this.getCustomers(1, this.search);
  }

  setPage(page: number) {
    this.getCustomers(page, this.search);
  }

  setPager(page: number) {
    if (page < 1 || page > this.pages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.pages, page);
  }
}
