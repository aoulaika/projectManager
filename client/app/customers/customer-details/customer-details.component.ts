import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";

import { CustomerService } from "../../services/customer.service";

@Component({
  selector: "app-customer-details",
  templateUrl: "./customer-details.component.html",
  styleUrls: ["./customer-details.component.scss"]
})
export class CustomerDetailsComponent implements OnInit {
  customer: {
    name: string;
    vat: string;
    zohoId?: string;
    discounts: { discount: number; createdAt: Date }[];
    country?: string;
    city?: string;
    state?: string;
    street?: string;
  };
  id: string;
  projects: {}[] = [];
  contacts: {}[] = [];
  isCustomerLoading = true;
  isProjectsLoading = true;
  isContactsLoading = true;
  customerForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private customerService: CustomerService
  ) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit() {
    const customerFormConfig = {
      name: [""],
      vat: [""],
      discounts: [""]
    };
    this.customerForm = this.fb.group(customerFormConfig);
    this.getCustomer(this.id);
    this.getCustomerProjects(this.id);
  }

  getCustomer(id: string): void {
    this.isCustomerLoading = true;
    this.customerService.getCustomer(id).subscribe(
      res => {
        this.customer = res;
        this.getContacts();
        this.setFormValues();
      },
      err => {
        console.error(err);
        this.isCustomerLoading = false;
      },
      () => {
        console.log("done");
        this.isCustomerLoading = false;
      }
    );
  }

  getContacts() {
    this.isContactsLoading = true;
    if (this.customer.zohoId) {
      this.customerService.getCustomerContacts(this.customer.zohoId).subscribe(
        res => {
          this.contacts = res.contacts;
        },
        err => {
          console.error(err);
          this.isContactsLoading = false;
        },
        () => {
          console.log("done");
          this.isContactsLoading = false;
        }
      );
    } else {
      this.isContactsLoading = false;
    }
  }

  getCustomerProjects(id: string): void {
    this.isProjectsLoading = true;
    this.customerService.getCustomerProjects(id).subscribe(
      res => {
        this.projects = res;
      },
      err => {
        console.error(err);
        this.isProjectsLoading = false;
      },
      () => {
        console.log("done");
        this.isProjectsLoading = false;
      }
    );
  }

  updateCustomer() {
    this.customerService.update(this.id, this.customerForm.value).subscribe(
      res => {},
      err => {
        console.error(err);
      }
    );
  }

  private setFormValues() {
    this.customerForm.setValue({
      name: this.customer.name || "",
      vat: this.customer.vat || "",
      discounts:
        this.customer.discounts.length > 0
          ? this.customer.discounts[0].discount
          : ""
    });
  }
}
