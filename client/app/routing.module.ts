import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {AuthGuardLogin} from './services/auth-guard-login.service';
import {FullLayoutComponent} from './full-layout/full-layout.component';
import {ProfileComponent} from './profile/profile.component';
import {UsersComponent} from './users/users.component';
import {SubsidiaryComponent} from './subsidiary/subsidiary.component';
import {CustomersComponent} from './customers/customers.component';
import {ProfilesComponent} from './profiles/profiles.component';
import { CustomerDetailsComponent } from './customers/customer-details/customer-details.component';
import { AuthGuardAdmin } from './services/auth-guard-admin.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'projects',
    pathMatch: 'full'
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [AuthGuardLogin],
    children: [
      {
        path: 'projects',
        loadChildren: './project/project.module#ProjectModule'
      },
      {
        path: 'profile',
        data: {
          title: 'Profile'
        },
        component: ProfileComponent
      },
      {
        path: 'users',
        data: {
          title: 'Users'
        },
        component: UsersComponent,
        canActivate: [AuthGuardAdmin]
      },
      {
        path: 'subsidiaries',
        data: {
          title: 'Subsidiaries'
        },
        component: SubsidiaryComponent,
        canActivate: [AuthGuardAdmin]
      },
      {
        path: 'customers/:id',
        data: {
          title: 'Customer Details'
        },
        component: CustomerDetailsComponent
      },
      {
        path: 'customers',
        data: {
          title: 'Customers'
        },
        component: CustomersComponent
      },
      {
        path: 'pricing',
        data: {
          title: 'Profiles'
        },
        component: ProfilesComponent,
        canActivate: [AuthGuardAdmin]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
