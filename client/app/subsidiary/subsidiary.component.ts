import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { SubsidiaryService } from "../services/subsidiary.service";
import { Validators } from "@angular/forms";

@Component({
  selector: "app-subsidiary",
  templateUrl: "./subsidiary.component.html",
  styleUrls: ["./subsidiary.component.scss"]
})
export class SubsidiaryComponent implements OnInit {
  subsidiaryForm: FormGroup;
  isLoading = false;
  subsidiaries: object[];
  collapse = false;

  constructor(
    private fb: FormBuilder,
    private subsidiaryService: SubsidiaryService
  ) {}

  ngOnInit() {
    const controlConfig = {
      name: ["", Validators.required],
      location: ["", Validators.required]
    };
    this.subsidiaryForm = this.fb.group(controlConfig);
    this.getSubsidiaries();
  }

  createSubsidiary() {
    this.subsidiaryService
      .createSubsidiary(this.subsidiaryForm.value)
      .subscribe(
        data => {
          this.subsidiaries.push(data);
        },
        error => {
          console.log(error);
        },
        () => {
          this.subsidiaryForm.reset();
        }
      );
    console.log(this.subsidiaryForm.value);
  }

  getSubsidiaries() {
    this.isLoading = true;
    this.subsidiaryService.getSubsidiaries().subscribe(
      data => {
        this.subsidiaries = data;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      },
      () => {}
    );
  }
}
