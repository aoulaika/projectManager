import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../services/profile.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {
  profilesForm: FormGroup;
  profiles= {
    architect: 0,
    mobile: 0,
    backend: 0,
    web: 0,
    designer: 0,
    tester: 0,
    manager: 0
  };

  constructor(private profileService: ProfileService, private fb: FormBuilder) {
  }

  ngOnInit() {
    const profilesFormConfig = {
      architect: [0],
      mobile: [0],
      backend: [0],
      web: [0],
      designer: [0],
      tester: [0],
      manager: [0]
    };
    this.profilesForm = this.fb.group(profilesFormConfig);
    this.getProfiles();
  }

  private getProfiles() {
    this.profileService.getProfiles()
      .subscribe(
        res => {
          res.forEach(profile => {
            this.profilesForm.get(profile.label).setValue(profile.budget);
            this.profiles[profile.label] = profile.budget;
          });
        },
        err => {
          console.log(err);
        },
        () => {
          console.log('done');
        },
      );
  }

  updateProfiles() {
    const profiles = this.getChangedValue();
    this.profileService.updateProfiles(profiles)
      .subscribe(
        data => {
          console.log(data);
        },
        err => {
          console.log(err);
        },
        () => {
          console.log('done');
        }
      );
  }

  private getChangedValue() {
    return Object.keys(this.profiles)
      .filter(key => this.profiles[key] !== this.profilesForm.value[key])
      .reduce((acc, key) => {
        acc[key] = this.profilesForm.value[key];
        return acc;
      }, {});
  }
}
