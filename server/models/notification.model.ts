import * as mongoose from 'mongoose';


const notificationSchema = new mongoose.Schema({
  name: String,
  message: String,
  scope: String,
  createdAt: {type: Date, default: Date.now},
  project: {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

const Notification = mongoose.model('Notification', notificationSchema);

export default Notification;
