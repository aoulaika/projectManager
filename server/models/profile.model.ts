import * as mongoose from 'mongoose';


const profileSchema = new mongoose.Schema({
  label: {type: String, enum: ['architect', 'mobile', 'backend', 'web', 'designer', 'tester', 'manager'], unique: true},
  budget: Number,
});

const Profile = mongoose.model('Profile', profileSchema);

export default Profile;
