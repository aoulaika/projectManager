import * as mongoose from 'mongoose';

const gdprSchema = new mongoose.Schema({
  data: {
    isEuropean: { type: Boolean },
    sensitivityLevel: {type: String, enum: ['standard data', 'personnal data', 'sensitive data']} ,
    isDataProcessor: {type: Boolean},
    clearOrEncrypted: {type: String, enum: ['', 'clear data', 'encrypted']}
  },
  hosting: {
    whoHostData: {type: String, enum: ['unsupported', 'client', '4d', 'third party']},
    hostinglink: { type: Boolean },
    askingHostingContract: { type: Boolean }
  },
  data_management: {
    isAccessingData: { type: Boolean },
    howDataAccess: {type: String, enum: ['', 'regie', 'remote', 'ftp']},
    howWillAccess: [{type: String, enum: ['', 'morocco', 'us', 'france', 'japon', 'australie', 'latin america']}]
  }
});

const Gdpr = mongoose.model('Gdpr', gdprSchema);

export default Gdpr;
