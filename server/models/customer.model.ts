import * as mongoose from "mongoose";

const customerSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  vat: { type: Number, min: 0, max: 100, default: 0 },
  discounts: {
    type: [
      {
        discount: { type: Number, min: 0, max: 100, default: 0 },
        createdAt: { type: Date, default: Date.now }
      }
    ],
    default: [{ discount: 0, createdAt: new Date() }]
  },
  createdAt: { type: Date, default: Date.now },
  zohoId: String
});

const Customer = mongoose.model("Customer", customerSchema);

export default Customer;
