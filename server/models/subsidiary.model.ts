import * as mongoose from 'mongoose';

const subsidiarySchema = new mongoose.Schema(
  {
    name: {type: String, required: true, unique: true},
    location: String
  }
);

const Subsidiary = mongoose.model('Subsidiary', subsidiarySchema);

export default Subsidiary;
