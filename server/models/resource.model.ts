import * as mongoose from 'mongoose';


const resourceSchema = new mongoose.Schema(
  {
    name: {type: String, required: true},
    email: {type: String, unique: true},
    role: {type: String, required: true, enum: ['admin', 'manager', 'employee']},
    zohoId: String,
    createdAt: {type: Date, default: Date.now}
  }
);

const Resource = mongoose.model('Resource', resourceSchema);

export default Resource;
