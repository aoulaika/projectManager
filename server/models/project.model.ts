import * as mongoose from "mongoose";

const projectSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  customer: { type: mongoose.Schema.ObjectId, ref: "Customer" },
  description: String,
  createdBy: { type: mongoose.Schema.ObjectId, ref: "User" },
  stakeholders: {
    service: { type: mongoose.Schema.ObjectId, ref: "User" },
    subsidiary: { type: mongoose.Schema.ObjectId, ref: "User" },
    manager: { type: mongoose.Schema.ObjectId, ref: "User" }
  },
  createdAt: { type: Date, default: Date.now },
  types: [
    { type: String, enum: ["4D", "Mobile", "4D Mobile", "Wakanda", "Other"] }
  ],
  language: String,
  technologies: String,
  notes: [
    {
      user: { type: mongoose.Schema.ObjectId, ref: "User" },
      note: String,
      date: { type: Date, default: Date.now }
    }
  ],
  attachments: [String],
  features: [
    {
      user: { type: mongoose.Schema.ObjectId, ref: "User" },
      feature: String,
      description: String,
      sprint: { type: mongoose.Schema.ObjectId },
      budget: String,
      date: { type: Date, default: Date.now }
    }
  ],
  sprints: [
    {
      user: { type: mongoose.Schema.ObjectId, ref: "User" },
      startDate: Date,
      endDate: Date,
      duration: Number
    }
  ],
  profiles: {
    architect: { type: Number, min: 0, max: 100 },
    mobile: { type: Number, min: 0, max: 100 },
    backend: { type: Number, min: 0, max: 100 },
    web: { type: Number, min: 0, max: 100 },
    designer: { type: Number, min: 0, max: 100 },
    tester: { type: Number, min: 0, max: 100 },
    manager: { type: Number, min: 0, max: 100 }
  },
  profilesBudget: [{
    label: {type: String, enum: ['architect', 'mobile', 'backend', 'web', 'designer', 'tester', 'manager']},
    budget: Number,
  }],
  resources: [
    {
      resource: { type: mongoose.Schema.ObjectId, ref: "Resource" },
      date: { type: Date, default: Date.now },
      budget: { type: Number, min: 0, max: 100 },
      role: {
        type: String,
        enum: [
          "architect",
          "mobile",
          "backend",
          "web",
          "designer",
          "tester",
          "manager"
        ]
      }
    }
  ],
  startDate: { type: Date },
  endDate: { type: Date },
  budget: String,
  status: [
    {
      label: {
        type: String,
        lowercase: true,
        trim: true,
        enum: [
          "new",
          "budgeted",
          "on-hold",
          "in-progress",
          "service validation",
          "manager validation",
          "completed",
          "canceled"
        ]
      },
      date: { type: Date, default: Date.now }
    }
  ],
  validations: [
    {
      process: Number,
      status: String,
      history: [
        {
          user: { type: mongoose.Schema.ObjectId, ref: "User" },
          validation: Boolean,
          comment: String,
          date: { type: Date, default: Date.now }
        }
      ]
    }
  ],
  step: [
    {
      type: String,
      lowercase: true,
      trim: true,
      enum: ["service", "subsidiary", "manager"]
    }
  ],
  zohoId: String,
  discount: { type: Number, default: 0, min: 0, max: 100 },
  potentialId: String,
  gdpr: { type: mongoose.Schema.ObjectId, ref: "Gdpr" },
},
{
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true 
  }
});

projectSchema.virtual('total').get(function() {
  let subTotal: number = 0; 
  let totalDiscount: number = 0;
  let totalVat: number = 0;
  let pf = this.profilesBudget;

  if(pf) {
    subTotal =   pf.reduce((acc, currentValue) => {
      return acc + currentValue.budget*this.profiles[currentValue.label];
    }, 0);
    subTotal = (subTotal || 0);
    totalDiscount = subTotal * (this.discount || 0) / 100;
    totalVat =  subTotal * (this.customer.vat || 0) / 100;
  }

  return subTotal - totalDiscount + totalVat;

});

const Project = mongoose.model("Project", projectSchema);

export default Project;
