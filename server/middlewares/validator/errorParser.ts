class Parser {
  parse = (errors) => {
    const parsedErrors = [];
    errors.forEach(error => parsedErrors.push(
      `body ${error.dataPath} ${error.message}`)
    );
    return {
      'error': {
        'message': 'invalid data',
        'data': parsedErrors
      }
    };
  }
}

const errorParser = new Parser();
export default errorParser;
