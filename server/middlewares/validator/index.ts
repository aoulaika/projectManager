const ajv = require('ajv')({v5: true, allErrors: true});

require('ajv-merge-patch')(ajv);
import schemas from './schemas';

import errorParser from './errorParser';

const note = {
  add: (req, res, next) => {
    validator.validateData('projectNote', req, res, next);
  }
};
const feature = {
  add: (req, res, next) => {
    validator.validateData('projectFeature', req, res, next);
  }
};

const resource = {
  add: (req, res, next) => {
    validator.validateData('projectResource', req, res, next);
  }
};

const project = {
  note: note,
  feature: feature,
  resource: resource,
  create: (req, res, next) => {
    validator.validateData('project', req, res, next);
  }
};

const validator = {
  project: project,
  resource: {},
  customer: {},
  feature: {},
  note: {},

  loadSchemas: () => {
    for (const prop in schemas) {
      if (schemas.hasOwnProperty(prop)) {
        ajv.addSchema(schemas[prop], prop);
      }
    }
  },

  validateData: (schemaId, req, res, next) => {
    const validate = ajv.compile(schemas[schemaId]);
    const body = req.body;
    const valid = validate(body);
    if (!valid) {
      res.status(412).json(errorParser.parse(validate.errors));
      return;
    }
    next();
  },
};

validator.loadSchemas();

export default validator;
