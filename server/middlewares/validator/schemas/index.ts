import * as fs from 'fs';

const schemas = {
  user: null,
  project: null,
  resource: null,
  notification: null,
  projectNote: null,
  projectType: null,
  projectFeature: null,
  projectResource: null,
  projectValidation: null
};

for (const prop in schemas) {
  if (schemas.hasOwnProperty(prop)) {
    const res = fs.readFileSync(
      `server/middlewares/validator/schemas/${prop}.json`).toString();
    schemas[prop] = JSON.parse(res);
  }
}

export default schemas;
