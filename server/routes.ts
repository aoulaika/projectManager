import * as express from "express";
import * as multer from "multer";

import ctrls from "./controllers";
import validator from "./middlewares/validator";

const subResources = {
  resourceId: "resource",
  featureId: "feature",
  noteId: "note",
  customerId: "customer"
};

export default function setRoutes(app) {
  const router = express.Router();

  router.param("projectId", function(req, res, next, projectId) {
    let resourceType = null;
    let resourceId = null;
    for (const key in subResources) {
      if (req.params.hasOwnProperty(key)) {
        resourceType = subResources[key];
        resourceId = req.params[key];
      }
    }

    ctrls.projectCtrl.exists(projectId, function(error, exists) {
      if (error) {
        const err = new Error("Server Error");
        return next(err);
      }
      if (!exists) {
        res.status(404).json({
          error: {
            message: `project '${projectId}' not found`
          }
        });
        return;
      }
      if (!resourceType) {
        next();
      }
    });
    if (resourceType) {
      ctrls[`${resourceType}Ctrl`].exists(resourceId, projectId, function(
        error,
        exists
      ) {
        if (error) {
          const err = new Error("Server Error");
          return next(err);
        }
        if (!exists) {
          res.status(404).json({
            error: {
              message: `${resourceType} '${resourceId}' not found`
            }
          });
          return;
        }
        next();
      });
    }
  });
  const storage = multer.diskStorage({
    destination: (req, file, cb) => cb(null, process.env.UPLOAD_DIR),
    filename: (req, file, cb) => cb(null, file.originalname)
  });
  const upload = multer({ storage: storage });

  // Auth routes
  router.route("/login").post( ctrls.userCtrl.getOne, ctrls.userCtrl.isAllowed, ctrls.authCtrl.createJWT);
  router
    .route("/zoho/customers")
    .post(ctrls.authCtrl.isAuthorized, ctrls.customerCtrl.insert);
  router.use(ctrls.authCtrl.isLoggedIn);
  router
    .route("/profile")
    .put(ctrls.userCtrl.updateProfile)
    .get(ctrls.userCtrl.getProfile);
  router
    .route("/changePassword")
    .post(ctrls.userCtrl.getOne, ctrls.authCtrl.changePassword);
  // User
  router
    .route("/users")
    .get(ctrls.userCtrl.getAll)
    .post(ctrls.userCtrl.insert);
  router
    .route("/users/:id")
    .get(ctrls.userCtrl.get)
    .put(ctrls.userCtrl.put)
    .delete(ctrls.userCtrl.delete);
    router
    .route("/users/:id/changeStatus")
    .put(ctrls.userCtrl.isAdmin, ctrls.userCtrl.changeStatus)


  

  // Project
  router
    .route("/projects")
    .get(ctrls.projectCtrl.getAll)
    .post(
      validator.project.create,
      ctrls.customerCtrl.getOrCreateCustomer,
      ctrls.zohoCtrl.createPotential,
      ctrls.projectCtrl.create
    );
  router.route("/projects/count").get(ctrls.projectCtrl.count);
  router
    .route("/projects/planning")
    .get(ctrls.projectCtrl.getProjectsPlanning)
    .post(ctrls.projectCtrl.getProjectsPlannings);

  router
    .route("/projects/:projectId")
    .get(ctrls.projectCtrl.get)
    .put(ctrls.projectCtrl.update)
    .delete(ctrls.projectCtrl.delete);

  router
    .route("/projects/:projectId/gdpr")
    .post(ctrls.projectCtrl.getOne, ctrls.gdprCtrl.saveGdpr)
    .get(ctrls.projectCtrl.getOne, ctrls.gdprCtrl.getGdpr);

  router
    .route("/projects/:projectId/status/:status")
    .put(ctrls.projectCtrl.getOne, ctrls.projectCtrl.updateStatus);
  router
    .route("/projects/:projectId/export")
    .get(ctrls.projectCtrl.exportExcel);

  // Project attachments
  router
    .route("/projects/:projectId/attachments")
    .post(upload.array("attachments"), ctrls.projectCtrl.upload);
  router
    .route("/projects/:projectId/attachments/:fileName")
    .get(ctrls.projectCtrl.downloadFile)
    .delete(ctrls.projectCtrl.removeFile);

  // Project notes
  router
    .route("/projects/:projectId/notes")
    .post(validator.project.note.add, ctrls.noteCtrl.addNote);
  router
    .route("/projects/:projectId/notes/:noteId")
    .delete(ctrls.noteCtrl.removeNote);

  // Project features
  router
    .route("/projects/:projectId/features")
    .post(
      validator.project.feature.add,
      ctrls.projectCtrl.getOne,
      ctrls.projectCtrl.isAuthorized,
      ctrls.featureCtrl.addFeature,
      ctrls.zohoCtrl.potentialNegotiation
    );
  router
    .route("/projects/:projectId/features/:featureId")
    .put(
      validator.project.feature.add,
      ctrls.projectCtrl.getOne,
      ctrls.projectCtrl.isAuthorized,
      ctrls.featureCtrl.updateFeature
    )
    .delete(ctrls.projectCtrl.getOne, ctrls.projectCtrl.isAuthorized, ctrls.featureCtrl.removeFeature);

  // Project sprints
  router
    .route("/projects/:projectId/sprints")
    .post(
      ctrls.projectCtrl.getOne,
      ctrls.projectCtrl.isAuthorized,
      ctrls.sprintCtrl.addSprint,
      ctrls.zohoCtrl.potentialNegotiation
    );
  router
    .route("/projects/:projectId/sprints/:sprintId")
    .put(ctrls.projectCtrl.getOne, ctrls.projectCtrl.isAuthorized, ctrls.sprintCtrl.updateSprint);

  // Project resources
  // validator.project.resource.add,
  router
    .route("/projects/:projectId/resources")
    .post(ctrls.projectCtrl.getOne, ctrls.projectCtrl.isAuthorized, ctrls.resourceCtrl.addResource);
  router
    .route("/projects/:projectId/resources/:resourceId")
    .delete(ctrls.projectCtrl.getOne, ctrls.projectCtrl.isAuthorized, ctrls.resourceCtrl.removeResource)
    .put(ctrls.projectCtrl.getOne, ctrls.projectCtrl.isAuthorized, ctrls.resourceCtrl.updateResource);

  // Project profiles
  router
    .route("/projects/:projectId/profiles")
    .post(
      ctrls.projectCtrl.getOne,
      ctrls.projectCtrl.isAuthorized,
      ctrls.projectCtrl.addProfiles,
      ctrls.zohoCtrl.potentialNegotiation
    )
    .get(ctrls.projectCtrl.getProjectProfiles);

  // Project validations
  router
    .route("/projects/:projectId/validations")
    .post(
      ctrls.projectCtrl.getOne,
      ctrls.validationCtrl.addValidation,
      ctrls.zohoCtrl.createProject
    );

  // Customer
  router
    .route("/customers")
    .get(ctrls.customerCtrl.getAll)
    .post(ctrls.zohoCtrl.createCustomer, ctrls.customerCtrl.insert);
  router
    .route("/customers/:id")
    .get(ctrls.customerCtrl.get)
    .put(ctrls.customerCtrl.update);
  router.route("/customers/:id/projects").get(ctrls.customerCtrl.getProjects);
  router
    .route("/customers/:id/contacts")
    .get(ctrls.zohoCtrl.getAccountContacts);

  // Resource
  router.route("/resources").get(ctrls.resourceCtrl.getAll);
  router
    .route("/resources/planning")
    .post(ctrls.resourceCtrl.getResourcesPlanning);

  // Subsidiaries
  router
    .route("/subsidiaries")
    .get(ctrls.subsidaryCtrl.getAll)
    .post(ctrls.subsidaryCtrl.create);

  // Profiles
  router
    .route("/profiles")
    .post(ctrls.profileCtrl.insert)
    .put(ctrls.profileCtrl.update)
    .get(ctrls.profileCtrl.getAll);

  // Profiles
  router.route("/notifications").get(ctrls.notificationCtrl.getAll);

  router.route("/test").get(ctrls.zohoCtrl.test);
  router.route("/projects/:zohoId/test").post(ctrls.zohoCtrl.test);

  // Apply the routes to our application with the prefix /api
  app.use("/api", router);
}
