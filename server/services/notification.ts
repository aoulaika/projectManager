import Notification from '../models/notification.model';

function addNotification(notification) {
  Notification.create(notification, (err, obj) => {
    if (err) {
      console.error(err);
      return false;
    }
    return true;
  });
}

export { addNotification };
