import axios from "axios";
import * as moment from "moment";
import { serializeData } from "./utils";

const url = "https://crm.zoho.com/crm/private/";
const params = { authtoken: process.env.ZOHO_CRM_AUTH_TOKEN };

export function fetchRecords() {
  return new Promise((resolve, reject) => {
    axios
      .get(`${url}json/Accounts/getRecords`, {
        params: { ...params, scope: "crmapi" }
      })
      .then(res => resolve(res))
      .catch(err => reject(err));
  });
}
export function createCustomer(account: {
  "Account Name": string;
  Phone?: string;
  Fax?: string;
}) {
  let accounts = '<Accounts><row no="1">';
  for (const key in account) {
    accounts += `<FL val="${key}">${account[key]}</FL>`;
  }
  accounts += "</row></Accounts>";
  return new Promise((resolve, reject) => {
    axios
      .post(`${url}json/Accounts/insertRecords`, null, {
        params: { ...params, scope: "crmapi", xmlData: accounts }
      })
      .then(res => {
        const response = res.data.response.result.recorddetail.FL;
        const id = response.find(fl => fl.val === "Id").content;
        resolve(id);
      })
      .catch(err => reject(err));
  });
}
export function getAccount(accountId: string) {
  return new Promise((resolve, reject) => {
    axios
      .get(`${url}json/Accounts/getRecordById`, {
        params: {
          ...params,
          scope: "crmapi",
          id: accountId,
          newFormat: 2,
          selectColumns:
            "Accounts(ACCOUNTID,Account Name,Email,Phone,Fax,Billing Street,Billing City,Billing State,Billing Country,NDA)"
        }
      })
      .then(res => {
        let response = [];
        if (!res.data.response.hasOwnProperty("nodata")) {
          response.push(res.data.response.result.Accounts.row);
          response = serializeData(response);
        }
        resolve(response[0]);
      })
      .catch(err => reject(err));
  });
}
export function getAccountContacts(accountId) {
  return new Promise((resolve, reject) => {
    axios
      .get(`${url}json/Contacts/getRelatedRecords`, {
        params: {
          ...params,
          scope: "crmapi",
          parentModule: "Accounts",
          id: accountId,
          newFormat: 2,
          selectColumns:
            "Contacts(CONTACTID,Email,Phone,Full Name,Mailing Street,Mailing City,Mailing State,Mailing Country)"
        }
      })
      .then(res => {
        let response = [];
        if (
          res.data.response.hasOwnProperty("result") &&
          res.data.response.result.hasOwnProperty("Contacts")
        ) {
          response = res.data.response.result.Contacts.row;
          if (!response.length) {
            response = [response];
          }
          response = serializeData(response);
        }
        resolve(response);
      })
      .catch(err => reject(err));
  });
}
export function createPotential(potential: {
  "Potential Name": string;
  ACCOUNTID: string;
  Stage?: string;
  "Closing Date"?: string;
}) {
  potential["Stage"] = "Open";
  potential["Closing Date"] = moment()
    .add(30, "days")
    .format("L");
  let potentials = '<Potentials><row no="1">';
  for (const key in potential) {
    potentials += `<FL val="${key}">${potential[key]}</FL>`;
  }
  potentials += "</row></Potentials>";
  return new Promise((resolve, reject) => {
    axios
      .post(`${url}json/Potentials/insertRecords`, null, {
        params: { ...params, scope: "crmapi", xmlData: potentials }
      })
      .then(res => {
        let response = [];
        if (!res.data.response.hasOwnProperty("nodata")) {
          console.log("res.data", res.data);
          response.push(res.data.response.result.recorddetail);
          response = serializeData(response);
        }
        resolve(response[0]["Id"]);
      })
      .catch(err => reject(err));
  });
}
export function potentialNegotiation(potentialId: string) {
  const potential = `<Potentials><row no="1"><FL val="Stage">Quote negotiation</FL></row></Potentials>`;
  return updatePotential(potentialId, potential);
}
export function potentialClosedWin(potentialId: string, budget: number) {
  const potential = `<Potentials><row no="1">
  <FL val="Stage">Closed won</FL>
  <FL val="Amount">${budget}</FL>
  </row></Potentials>`;
  return updatePotential(potentialId, potential);
}
export function potentialClosedLost(potentialId: string) {
  const potential = `<Potentials><row no="1"><FL val="Stage">Closed lost</FL></row></Potentials>`;
  return updatePotential(potentialId, potential);
}
export function updatePotential(potentialId: string, potential) {
  return new Promise((resolve, reject) => {
    axios
      .post(`${url}json/Potentials/updateRecords`, null, {
        params: {
          ...params,
          scope: "crmapi",
          id: potentialId,
          xmlData: potential
        }
      })
      .then(res => {
        return resolve(res);
      })
      .catch(err => reject(err));
  });
}
export function createLicense(license) {
  let licenses = '<CustomModule1><row no="1">';
  for (const key in license) {
    licenses += `<FL val="${key}">${license[key]}</FL>`;
  }
  licenses += "</row></CustomModule1>";
  return new Promise((resolve, reject) => {
    axios
      .post(`${url}xml/CustomModule1/insertRecords`, null, {
        params: { ...params, scope: "crmapi", xmlData: licenses }
      })
      .then(res => resolve(res))
      .catch(err => reject(err));
  });
}
