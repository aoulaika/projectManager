import * as winston from "winston";
require("winston-daily-rotate-file");
import { default as loggerConfig } from "../config/logger";

const logger = {
  debug: null,
  info: null,
  warn: null,
  error: null
};

function toLengthened(word, length) {
  for (let i = word.length; i < length; i++) {
    word += " ";
  }
  return word;
}

function maskPassword(data) {
  if (typeof data === "object" && !Array.isArray(data)) {
    let newObject = Object.assign({}, data);
    if (newObject.password) {
      newObject.password = "******";
    }
    for (let i in newObject) {
      newObject[i] = maskPassword(newObject[i]);
    }
    return newObject;
  } else if (Array.isArray(data)) {
    let newArray = data.slice();
    newArray.forEach(function(obj, index) {
      newArray[index] = maskPassword(obj);
    });
    return newArray;
  }
  return data;
}

const fileTransport = new winston.transports.DailyRotateFile({
  name: "projectManagerLogFile",
  filename: loggerConfig.filename,
  level: loggerConfig.DEBUG === true ? "debug" : "info",
  datePattern: "yyyy-MM-dd.",
  prepend: true,
  maxFiles: 7,
  json: false,
  timestamp: function() {
    return new Date().toJSON();
  },

  formatter: function(options) {
    return (
      options.timestamp() +
      "    " +
      toLengthened(options.level.toUpperCase(), 9) +
      (options.message !== undefined ? options.message : "") +
      (options.meta.data && Object.keys(options.meta.data).length
        ? " -- " + JSON.stringify(maskPassword(options.meta.data))
        : "")
    );
  }
});

const winstonLogger = new winston.Logger({
  transports: [fileTransport]
});

logger.debug = function(entity, message, data) {
  if (loggerConfig.DEBUG) {
    winstonLogger.debug(toLengthened(entity.toUpperCase(), 24) + message, {
      data: data
    });
  }
};
logger.info = function(entity, message, data) {
  winstonLogger.info(toLengthened(entity.toUpperCase(), 24) + message, {
    data: data
  });
};
logger.warn = function(entity, message, data) {
  winstonLogger.warn(toLengthened(entity.toUpperCase(), 24) + message, {
    data: data
  });
};
logger.error = function(entity, message, data) {
  winstonLogger.error(toLengthened(entity.toUpperCase(), 24) + message, {
    data: data
  });
};
export {winstonLogger};
export default logger;
