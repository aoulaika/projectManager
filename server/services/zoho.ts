import axios from "axios";
import * as moment from "moment";
import Resource from "../models/resource.model";

const url = "https://projectsapi.zoho.com/restapi/portal/ayoubolk/projects/";
const PORTALID = "ayoubolk";
const params = { authtoken: process.env.ZOHO_AUTH_TOKEN };

function addProject(project_name) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, null, { params: { ...params, name: project_name } })
      .then(res => resolve(res.data.projects))
      .catch(err => reject(err));
  });
}

/**
 * create task lists except backlog
 * @param projectId 
 */
function createTasks(projectId) {
  const uri = `${url}${projectId}/tasklists/`;
  return new Promise((resolve, reject) => {
    axios
      .all([
        axios.post(uri, null, {
          params: { ...params, name: "Open", flag: "internal" }
        }),
        axios.post(uri, null, {
          params: { ...params, name: "In-progress", flag: "internal" }
        }),
        axios.post(uri, null, {
          params: { ...params, name: "Testing", flag: "internal" }
        }),
        axios.post(uri, null, {
          params: { ...params, name: "Done", flag: "internal" }
        })
      ])
      .then(() => {
        resolve({});
      })
      .catch(err => reject(err));
  });
}

function createBackLogTaskList(projectId) {
  return new Promise((resolve, reject) => {
    axios
      .post(`${url}${projectId}/tasklists/`, null, {
        params: { ...params, name: "Backlog", flag: "internal" },
        responseType: "json"
      })
      .then(response => {
        resolve(response.data.tasklists);
      })
      .catch(err => reject(err));
  });
}

async function createBackLogTasks(projectId, taskListId, features) {
  for (const f of features) {
    await axios.post(`${url}${projectId}/tasks/`, null, {
      params: {
        ...params,
        name: f.feature,
        tasklist_id: taskListId,
        description: f.description
      },
      responseType: "json"
    });
  }
  return new Promise((resolve, reject) => resolve({ msg: "!!!" }));
}

async function addResources(projectId, resources) {
  const manager = resources
    .filter(r => r.role === "manager")
    .map(r => r.resource.email)
    .join(",");
  if (manager) {
    await axios.post(`${url}${projectId}/users/`, null, {
      params: { ...params, email: manager, role: "manager" }
    });
  }
  const employee = resources
    .filter(r => r.role !== "manager")
    .map(r => r.resource.email)
    .join(",");
  if (employee) {
    await axios.post(`${url}${projectId}/users/`, null, {
      params: { ...params, email: employee, role: "employee" }
    });
  }
  return new Promise((resolve, reject) => resolve({ done: true }));
}

async function addMilestones(projectId, ownerId, sprints) {
  const uri = `${url}${projectId}/milestones/`;
  const promises = sprints.map((sprint, index) => {
    const _params = {
      ...params,
      name: `Sprint ${index}`,
      start_date: moment(sprint.startDate).format("MM-DD-YYYY"),
      end_date: moment(sprint.endDate).format("MM-DD-YYYY"),
      owner: ownerId,
      flag: "internal"
    };
    return axios.post(uri, null, { params: _params });
  });
  return new Promise((resolve, reject) => {
    axios
      .all(promises)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export {
  addProject,
  createTasks,
  createBackLogTaskList,
  createBackLogTasks,
  addResources,
  addMilestones
};