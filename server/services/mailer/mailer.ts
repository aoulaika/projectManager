import * as hbs from "nodemailer-express-handlebars";
import * as exhbs from "express-handlebars";
import * as nodemailer from "nodemailer";
import { resolve } from "path";

import ctrls from "../../controllers/index";

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "service.4d.projects@gmail.com",
    pass: "4dprojects123"
  }
});
const ehbs = exhbs.create();
const handlebarsOptions = {
  viewEngine: ehbs,
  viewPath: resolve("server/services/mailer/templates/"),
  extName: ".hbs"
};
const mailOptionsDefault = {
  from: "service.4d.projects@gmail.com", // sender address
  to: "ayoubolk@gmail.com", // list of receivers
  subject: "Connect to 4d projects" // Subject line
};

transporter.use("compile", hbs(handlebarsOptions));

export function sendMail(mailOptions) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function(err, info) {
      if (err) reject(err);
      else resolve(info);
    });
  });
}
/**
 * notify the user when account created
 * @param data
 */
export function createUser(data: {
  email: string;
  password: string;
  name: string;
}) {
  const mailOptions = {
    ...mailOptionsDefault,
    to: data.email,
    subject: "Your 4D projects access",
    template: "new-user",
    context: { user: data }
  };
  return sendMail(mailOptions);
}
/**
 * notify service's users when new project created
 * @param data
 */
export function createProject(
  data: {
    name: string;
    owner: string;
    url: string;
  },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  if (stakeholders && "service" in stakeholders && stakeholders.service) {
    console.log(stakeholders);
    ctrls.userCtrl
      .getEmailById([stakeholders.service])
      .then(email => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: email,
          subject: "A new project has been created",
          template: "new-project",
          context: {
            user: { name: data.owner },
            project: { name: data.name, url: data.url }
          }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getEmailsByRole({ role: "service" })
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "A new project has been created",
          template: "new-project",
          context: {
            user: { name: data.owner },
            project: { name: data.name, url: data.url }
          }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}

/**
 *
 * @param project
 * @param user
 * @param validation
 */
export function projectValidated(
  project: { name: string; id: string; createdBy: string },
  user: { role: string },
  validation?: { msg: string },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  const url = `${process.env.SERVER_NAME}/${project.id}`;
  switch (user.role) {
    case "subsidiary":
      subsidiaryValidation({ name: project.name, url }, stakeholders);
      break;

    case "service":
      serviceValidation({ name: project.name, url }, stakeholders);
      break;

    case "manager":
      managerValidation(
        { name: project.name, url },
        { id: project.createdBy },
        stakeholders
      );
      break;

    default:
      break;
  }
}

/**
 * notify service when subsidiary validate project
 * @param data
 */
function subsidiaryValidation(
  data: { name: string; url: string },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  if (stakeholders && "service" in stakeholders && stakeholders.service) {
    const ids = [stakeholders.service];
    ctrls.userCtrl
      .getEmailById(ids)
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "subsidiary-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getEmailsByRole({ role: "service" })
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "subsidiary-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
/**
 * notify managers when service validate a project
 * @param data
 */
function serviceValidation(
  data: { name: string; url: string },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  if (stakeholders && "manager" in stakeholders && stakeholders.manager) {
    const ids = [stakeholders.manager];
    ctrls.userCtrl
      .getEmailById(ids)
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "subsidiary-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getEmailsByRole({ role: "manager" })
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "service-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
/**
 * notify all linked users to the project
 * @param data
 */
function managerValidation(
  data: { name: string; url: string },
  user: { id: string },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  const ids = [];
  if (stakeholders && "service" in stakeholders && stakeholders.service) {
    ids.push(stakeholders.service);
  }
  if (stakeholders && "subsidiary" in stakeholders && stakeholders.subsidiary) {
    ids.push(stakeholders.subsidiary);
  }
  if (ids) {
    ctrls.userCtrl
      .getEmailById(ids)
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "subsidiary-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getById(user.id)
      .then(res =>
        ctrls.userCtrl.getEmailsByRole({
          $or: [
            { role: "service" },
            { role: "subsidiary", subsidiary: res.subsidiary }
          ]
        })
      )
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been validated",
          template: "manager-validation",
          context: { project: { name: data.name, url: data.url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
/**
 *
 * @param data
 */
export function budgeting(
  project: {
    name: string;
    id: string;
    createdBy: string;
  },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  if (stakeholders && "subsidiary" in stakeholders && stakeholders.subsidiary) {
    const ids = [stakeholders.subsidiary];
    ctrls.userCtrl
      .getEmailById(ids)
      .then(emails => {
        const url = `${process.env.SERVER_NAME}/${project.id}`;
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been budgeted",
          template: "project-budgeted",
          context: { project: { name: project.name, url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getById(project.createdBy)
      .then(res =>
        ctrls.userCtrl.getEmailsByRole({
          role: "subsidiary",
          subsidiary: res.subsidiary
        })
      )
      .then(emails => {
        const url = `${process.env.SERVER_NAME}/${project.id}`;
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been budgeted",
          template: "project-budgeted",
          context: { project: { name: project.name, url } }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}

/**
 *
 * @param project
 * @param user
 * @param validation
 */
export function projectRejected(
  project: { name: string; id: string; createdBy: string },
  user: { role: string },
  validation?: { msg: string },
  stakeholders?: { service?: string; subsidiary?: string; manager?: string }
) {
  const url = `${process.env.SERVER_NAME}/${project.id}`;
  const ids = [];
  if (
    user.role == "service" &&
    stakeholders &&
    "subsidiary" in stakeholders &&
    stakeholders.subsidiary
  ) {
    ids.push(stakeholders.subsidiary);
  } else if (user.role == "manager") {
    if (stakeholders && "service" in stakeholders && stakeholders.service) {
      ids.push(stakeholders.service);
    }
    if (
      stakeholders &&
      "subsidiary" in stakeholders &&
      stakeholders.subsidiary
    ) {
      ids.push(stakeholders.subsidiary);
    }
  }
  if (ids) {
    ctrls.userCtrl
      .getEmailById(ids)
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been rejected",
          template: "reject-validation",
          context: {
            project: {
              name: project.name,
              url: url,
              validation: { comment: validation.msg }
            }
          }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    ctrls.userCtrl
      .getById(project.createdBy)
      .then(res => {
        let criteria = {};
        if (user.role == "service") {
          criteria = { role: "subsidiary", subsidiary: res.subsidiary };
        } else if (user.role == "manager") {
          criteria = {
            $or: [
              { role: "service" },
              { role: "subsidiary", subsidiary: res.subsidiary }
            ]
          };
        }
        return ctrls.userCtrl.getEmailsByRole(criteria);
      })
      .then(emails => {
        const mailOptions = {
          ...mailOptionsDefault,
          to: emails,
          subject: "Project has been rejected",
          template: "reject-validation",
          context: {
            project: {
              name: project.name,
              url: url,
              validation: { comment: validation.msg }
            }
          }
        };
        return sendMail(mailOptions);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
