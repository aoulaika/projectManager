const moment = require("moment");

export function countWeekendDays(d0, d1) {
  const ndays =
    1 + Math.round((d1.getTime() - d0.getTime()) / (24 * 3600 * 1000));
  const nsaturdays = Math.floor((d0.getDay() + ndays) / 7);
  return (
    2 * nsaturdays + (d0.getDay() === 0 ? 1 : 0) - (d1.getDay() === 6 ? 1 : 0)
  );
}

export function getEndDate(startDate, duration) {
  let endDate = moment(startDate)
    .add(duration, "d")
    .toDate();
  let weekendDaysBase, weekendDays;
  do {
    weekendDaysBase = countWeekendDays(startDate, endDate);
    endDate = moment(startDate)
      .add(duration + weekendDaysBase, "d")
      .toDate();
    weekendDays = countWeekendDays(startDate, endDate);
  } while (weekendDays - weekendDaysBase > 0);
  return endDate;
}
export function serializeData(arr: { FL: { val; content }[] }[]) {
  return arr.map(row => {
    return row.FL.reduce((acc, contact) => {
      acc[contact.val] = contact.content;
      return acc;
    }, {});
  });
}
