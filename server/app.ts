import * as bodyParser from "body-parser";
import * as dotenv from "dotenv";
import * as express from "express";
import * as morgan from "morgan";
import * as mongoose from "mongoose";
import * as path from "path";
import * as jwt from "jsonwebtoken";
import * as cookieParse from "cookie-parser";
import * as SocketServer from "socket.io";
import * as cors from "cors";
import * as expressWinston from "express-winston";

dotenv.load({ path: ".env" });
import ctrls from "./controllers";
import setRoutes from "./routes";
import { addNotification } from "./services/notification";
import { winstonLogger } from "./services/logger";

const app = express();
app.set("port", process.env.PORT || 3000);
app.set("hostname", process.env.HOSTNAME || "localhost");

app.use(cors());

app.use("/", express.static(path.join(__dirname, "../public")));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParse());
app.use((req, res, next) => {
  if (req.headers && req.cookies) {
    jwt.verify(req.cookies.Session, process.env.SECRET_TOKEN, (err, decode) => {
      req.user = decode ? decode.user : null;
      next();
    });
  } else {
    req.user = undefined;
    next();
  }
});
mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
const db = mongoose.connection;
(<any>mongoose).Promise = global.Promise;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
  app.use(morgan("dev"));
  app.use(
    expressWinston.logger({
      winstonInstance: winstonLogger,
      colorize: true,
      dumpExceptions: true,
      showStack: true,
      msg:
        "{{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms"
    })
  );
  setRoutes(app);

  app.get("/*", (req, res) => {
    res.sendFile(path.join(__dirname, "../public/index.html"));
  });
  app.use(
    expressWinston.errorLogger({
      winstonInstance: winstonLogger,
      colorize: true,
      dumpExceptions: true,
      showStack: true,
      msg: "{{req.method}} {{req.url}} {{res.statusCode}} {{err.message}}"
    })
  );

  const server = app.listen(app.get("port"), app.get("hostname"), () => {
    console.log(
      `Project manager app listening on port http://${app.get("hostname")}:${app.get("port")}`
    );
  });

  const socketServer = SocketServer.listen(server);

  socketServer.on("connection", socket => {
    socket
      .on("userLoggedIn", userEmail => {
        console.log(userEmail);
        ctrls.userCtrl
          .getByEmail(userEmail)
          .then(user => {
            if (["manager", "service"].includes(user.role)) {
              socket.join(user.role);
            }
            socket.join(user.subsidiary);
            console.log(user.role);
            socket.emit(
              "welcomeUserLoggedIn",
              `Hey ${user.firstName} .. Welcome back!`
            );
          })
          .catch(err => {
            console.log(err);
          });
      })
      .on("projectCreated", projectName => {
        ctrls.projectCtrl
          .getByName(projectName)
          .then(project => {
            ctrls.userCtrl.getById(project.createdBy).then(user => {
              const message = `A new project named "${projectName}" has been created`;
              const notif = {
                name: "projectCreated",
                message: message,
                createdAt: Date.now(),
                scope: user.role == "subsidiary" ? user.subsidiary : user.role,
                project: project._id,
                user: user._id
              };
              console.log(JSON.stringify(notif));
              addNotification(notif); // add this notification to the database
              socket.broadcast
                .to("service")
                .emit("projectCreated", { message: message, notif: notif });
              if (user.role == "subsidiary") {
                socket.broadcast
                  .to(user.subsidiary)
                  .emit("projectCreated", { message: message, notif: notif });
              }
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .on("projectBudgeted", projectName => {
        ctrls.projectCtrl
          .getByName(projectName)
          .then(project => {
            ctrls.userCtrl.getById(project.createdBy).then(user => {
              const message = `The project named "${projectName}" has been budgeted`;
              const notif = {
                name: "projectBudgeted",
                message: message,
                createdAt: Date.now(),
                scope: user.role == "subsidiary" ? user.subsidiary : user.role,
                project: project._id,
                user: user._id
              };
              addNotification(notif); // add this notification to the database
              socket.broadcast
                .to("service")
                .emit("projectBudgeted", { message: message, notif: notif });
              if (user.role == "subsidiary") {
                socket.broadcast
                  .to(user.subsidiary)
                  .emit("projectBudgeted", { message: message, notif: notif });
              }
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .on("projectValidated", projectName => {
        ctrls.projectCtrl
          .getByName(projectName)
          .then(project => {
            ctrls.userCtrl.getById(project.createdBy).then(user => {
              const message = `The project named "${projectName}" has been validated`;
              const notif = {
                name: "projectValidated",
                message: message,
                createdAt: Date.now(),
                scope: user.role == "subsidiary" ? user.subsidiary : user.role,
                project: project._id,
                user: user._id
              };
              console.log(JSON.stringify(notif));
              addNotification(notif); // add this notification to the database
              socket.broadcast
                .to("service")
                .emit("projectValidated", { message: message, notif: notif });
              socket.broadcast
                .to("manager")
                .emit("projectValidated", { message: message, notif: notif });
              if (user.role == "subsidiary") {
                socket.broadcast
                  .to(user.subsidiary)
                  .emit("projectValidated", { message: message, notif: notif });
              }
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .on("zohoProjectCreated", projectName => {
        ctrls.projectCtrl
          .getByName(projectName)
          .then(project => {
            ctrls.userCtrl.getById(project.createdBy).then(user => {
              const message = `The project named "${projectName}" has been created on Zoho`;
              const notif = {
                name: "zohoProjectCreated",
                message: message,
                createdAt: Date.now(),
                scope: user.role,
                project: project._id,
                user: user._id
              };
              addNotification(notif); // add this notification to the database
              socket.broadcast
                .to(user.role)
                .emit("zohoProjectCreated", { message: message, notif: notif });
            });
          })
          .catch(err => {
            console.log(err);
          });
      });
  });
});

export { app };
