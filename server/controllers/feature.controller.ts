import * as mongoose from "mongoose";
import BaseCtrl from "./base";
import Project from "../models/project.model";

export default class FeatureCtrl extends BaseCtrl {
  model = Project;

  exists = (featureId, projectId, callback) => {
    this.model.findOne({ _id: projectId }, function(err, res) {
      const features = res.features.filter(
        feature => feature._id.toString() === featureId
      );
      if (features.length > 0) {
        callback(null, true);
      } else {
        callback(err);
      }
    });
  };

  // Project features management
  addFeature = (req, res, next) => {
    const user = req.user;
    const body = req.body;
    const project = req.project;
    const isBudgeted =
      project.status.filter(s => s.label === "budgeted").length === 1;
    if (!isBudgeted) {
      project.status.push({
        label: "budgeted",
        date: Date.now()
      });
    }
    const feature = {
      _id: new mongoose.Types.ObjectId(),
      feature: body.feature,
      description: body.description,
      budget: body.budget,
      sprint: body.sprint,
      date: Date.now(),
      user: user._id
    };
    project.features.push(feature);
    project
      .save()
      .then(p => {
        feature["user"] = {
          _id: user._id,
          firstName: user.firstName,
          lastName: user.lastName
        };
        if (!isBudgeted) {
          req.data = feature;
          return next();
        } else {
          return res.status(201).json(feature);
        }
      })
      .catch(err => {
        res.status(500).end();
      });
  };

  updateFeature = (req, res) => {
    const feature = req.body;
    const project = req.project;
    project.features = project.features.map(f => {
      if (f._id.toString() === req.params.featureId) {
        f.feature = feature.feature;
        f.description = feature.description;
        f.budget = feature.budget;
        f.sprint = feature.sprint || null;
        return f;
      }
      return f;
    });
    project
      .save()
      .then(obj => res.json(obj.features))
      .catch(err => console.error(err));
  };

  removeFeature = (req, res) => {
    this.model
      .update(
        { _id: req.params.projectId },
        { $pull: { features: { _id: req.params.featureId } } }
      )
      .then(obj => res.json(obj))
      .catch(err => console.error(err));
  };
}
