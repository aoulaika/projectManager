import BaseCtrl from "./base";
import Gdpr from "../models/gdpr.model";

export default class GdprCtrl extends BaseCtrl {
  model = Gdpr;

  saveGdpr = async (req, res) => {
    let savedGdpr;
    const gdprData = req.body;
    const newGdpr = new this.model(gdprData);
    try {
      savedGdpr = await newGdpr.save();
      req.project.gdpr = savedGdpr.id;
      await req.project.save();
    } catch (e) {
      return res.status(400).json({ message: e.message});
    }
    return res.status(200).json(savedGdpr);
  };

  getGdpr = async (req, res) => {
    return res.status(200).json(req.project.gdpr);
  };
}

