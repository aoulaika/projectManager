import * as moment from "moment";
import * as mongoose from "mongoose";

import BaseCtrl from "./base";
import Project from "../models/project.model";
import { countWeekendDays } from "../services/utils";
import * as zohoCRM from "../services/zoho-crm";

export default class SprintCtrl extends BaseCtrl {
  model = Project;

  addSprint = (req, res, next) => {
    const body = req.body;
    const isBudgeted =
      req.project.status.filter(s => s.label === "budgeted").length === 1;
    if (!isBudgeted) {
      req.project.status.push({
        label: "budgeted",
        date: Date.now()
      });
    }
    let duration =
      moment(body.endDate).diff(moment(body.startDate), "days") + 1;
    duration -= countWeekendDays(
      new Date(body.startDate),
      new Date(body.endDate)
    );
    body.duration = duration;
    body.user = req.user._id;
    body._id = new mongoose.Types.ObjectId();
    req.project["sprints"].push(body);
    req.project
      .save()
      .then(p => {
        body.user = req.user;
        if (!isBudgeted) {
          req.data = body;
          return next();
        } else {
          return res.status(201).json(body);
        }
      })
      .catch(err => console.error(err));
  };

  updateSprint = (req, res) => {
    const body = req.body;
    let duration =
      moment(body.endDate).diff(moment(body.startDate), "days") + 1;
    duration -= countWeekendDays(
      new Date(body.startDate),
      new Date(body.endDate)
    );
    body.duration = duration;
    req.project["sprints"] = req.project["sprints"].map(sprint => {
      if (sprint._id.toString() === req.params.sprintId) {
        sprint.startDate = body.startDate;
        sprint.endDate = body.endDate;
        sprint.duration = duration;
      }
      return sprint;
    });
    req.project
      .save()
      .then(project => {
        res.json(project.sprints);
      })
      .catch(err => console.error(err));
  };
}
