import * as mongoose from 'mongoose';
import * as moment from 'moment';
import * as randomColor from 'randomcolor';


import BaseCtrl from './base';
import Project from '../models/project.model';
import Resource from '../models/resource.model';
import { getEndDate } from '../services/utils';

export default class ResourceCtrl extends BaseCtrl {
  model = Resource;
  project = Project;

  exists = (resourceId, projectId, callback) => {
    this.project.findOne({ _id: projectId }, function (err, res) {
      const resources = res.resources.filter(r => r.resource.toString() === resourceId);
      console.log(resources.length);
      if (resources.length > 0) {
        callback(null, true);
      } else {
        callback(err);
      }
    });
  }

  // Project resources management
  addResource = (req, res) => {
    const user = req.user;
    const body = req.body;
    const resource = {
      _id: new mongoose.Types.ObjectId,
      resource: body.resource,
      date: body.date,
      user: user._id
    };
    this.project.update({ _id: req.params.projectId, 'resources.resource': { $ne: resource.resource } }, {
      $push: {
        resources: resource
      }
    }, (err, obj) => {
      if (err) {
        res.status(500).end();
      } else if (obj.nModified === 0) {
        res.status(409).end();
      } else {
        resource['user'] = {
          _id: user._id,
          firstName: user.firstName,
          lastName: user.lastName,
        };
        res.status(201).json(resource);
      }
    });
  }

  updateResource = (req, res) => {
    const project = req.project;
    const role = req.body.role;
    const profiles = project.profiles || {};
    const resources = project.resources || [];
    const sameRoleCount = resources.filter(resource => resource.role === role);
    const budget = profiles[role] / (1 + sameRoleCount.length);
    project.resources = resources.map(r => {
      if (r.role === role) {
        r.budget = budget;
      }
      if (r.resource._id.toString() === req.params.resourceId) {
        r.role = role;
        r.budget = budget;
      }
      return r;
    });
    project.save().then(obj => {
      res.json(req.params);
    });
  }

  removeResource = (req, res) => {
    // TODO: modify response and manage exception
    const project = req.project;
    project.resources = project.resources.filter(r => r.resource._id.toString() !== req.params.resourceId);
    console.log(project.resources);
    const count = {};
    Object.keys(project.profiles).forEach(p => {
      const sum = project.resources.reduce((s, r) => r.role === p ? s + 1 : s, 0);
      if (sum > 0) {
        count[p] = sum;
      }
    });
    Object.keys(count).forEach(p => {
      project.resources = project.resources.map(r => {
        if (r.role === p) {
          r.budget = project.profiles[p] / count[p];
        }
        return r;
      });
    });
    project.save()
      .then(obj => {
        res.json(obj);
      });
  }

  getResourcesPlanning = (req, res) => {
    console.log(req.body.data);
    // return res.json({});
    this.project.find().where('resources.resource').in(req.body.data).populate([{path: 'resources.resource'}]).exec()
      .then(data => {
        // data contain all projects which the selected resources are linked to
        // we should get work percentage of each resource on each project
        let result = data.map(project => {

          let projectDuration = project.sprints.reduce((sum, sprint) => +sprint.duration + sum, 0);

          const budgets = project.profiles;
          
          const profilesSum = budgets.architect + budgets.designer + budgets.tester + budgets.manager +
            Math.max(budgets.mobile, budgets.backend, budgets.web);

          projectDuration = projectDuration === 0 ? profilesSum : projectDuration;

          return project.resources.filter(resource => req.body.data.includes(resource.resource._id.toString()))
            .map(resource => {
              if (!resource.budget) {
                // should be ignored
              }              
              const percentage = (100 * resource.budget / profilesSum).toFixed(2);
              return {
                id: project._id,
                title: `${project.name} (${resource.resource.name} ${percentage}%)`,
                start: project.startDate,
                end: getEndDate(project.startDate, projectDuration),
                allDay: true,
                color: randomColor({ luminosity: 'light', format: 'rgb', hue: 'blue' }),
                className: ['text-uppercase', 'text-dark'],
              };
            });
        });
        result = [].concat(...result);
        res.json(result);
      })
      .catch(err => {
        console.log(err);
        res.json({});
      });
  }
}
