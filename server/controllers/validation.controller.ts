import logger from "../services/logger";
import * as mailer from "../services/mailer/mailer";

import BaseCtrl from "./base";
import Project from "../models/project.model";

export default class ValidationCtrl extends BaseCtrl {
  model = Project;

  addValidation = (req, res, next) => {
    const roles = ["subsidiary", "service", "manager"];
    const user = req.user;
    const body = req.body;
    if (req.project.step.length != roles.indexOf(user.role)) {
      return res.status(403).json({});
    }
    let shouldCreateInZoho = false;
    const lastProcess = req.project.validations.slice(-1).pop();
    if (!lastProcess) {
      req.project.validations.push({
        status: "Not valide",
        history: [
          {
            user: user._id,
            validation: body.validation,
            comment: body.comment,
            date: Date.now()
          }
        ]
      });
    } else {
      const valid = lastProcess.history.filter(v => v.validation);
      if (valid.length === 3) {
        return res.status(409).json({
          error: {
            message: `Project ${req.params.projectId} already valide!`
          }
        });
      }
      const notValid =
        lastProcess.history.filter(v => !v.validation).length >= 1;
      if (notValid) {
        req.project.validations.push({
          status: "Not valide",
          history: []
        });
      } else {
        const roleAlreadyValidate =
          lastProcess.history.filter(v => v.user.role === user.role).length >=
          1;
        if (roleAlreadyValidate) {
          return res.status(403).json({
            error: {
              message: `Cannot have two validations from ${user.role}`
            }
          });
        }
        const userAlreadyValidate =
          valid.filter(v => v.user._id.toString() === user._id).length >= 1;
        if (userAlreadyValidate) {
          return res
            .status(403)
            .json({ error: { message: "Cannot validate the project twice!" } });
        }
      }
      req.project.validations[req.project.validations.length - 1].history.push({
        user: user._id,
        validation: body.validation,
        comment: body.comment,
        date: Date.now()
      });
      if (!body.validation) {
        req.project.validations[req.project.validations.length - 1].status =
          "Not valide";
      } else if (valid.length === 2 && !notValid) {
        req.project.validations[req.project.validations.length - 1].status =
          "Valide";
        shouldCreateInZoho = true;
      }
    }
    if (body.validation) {
      req.project.step.push(user.role);
      if (user.role != "manager") {
        req.project.status.push({
          label: `${roles[roles.indexOf(user.role) + 1]} validation`,
          date: Date.now()
        });
      }
    } else {
      req.project.step = [];
      req.project.status.push({ label: "budgeted", date: Date.now() });
    }
    if (shouldCreateInZoho) {
      return next();
    } else {
      req.project
        .save()
        .then(() => {
          const data = { name: req.project.name, id: req.project._id, createdBy: req.project.createdBy };
          if (body.validation) {
            mailer.projectValidated(data, {role: user.role}, req.project.stakeholders);
          } else {
            mailer.projectRejected(data, {role: user.role}, {msg: body.comment}, req.project.stakeholders);
          }
          res.json({});
        })
        .catch(err => {
          res.status(500).json({ error: { message: err.message } });
        });
    }
  };
}
