import Customer from "../models/customer.model";
import Project from "../models/project.model";
import User from "../models/user.model";
import BaseCtrl from "./base";
import * as zohoCRM from "../services/zoho-crm";
import logger from "../services/logger";

export default class CustomerCtrl extends BaseCtrl {
  model = Customer;
  entity = "customer";
  userModel = User;
  projectModel = Project;

  exists = (customerId, projectId, callback) => {
    this.projectModel.find({ _id: projectId }, function(err, res) {
      const notes = res[0].notes.filter(customer => customer._id == customerId);
      console.log(notes.length);
      if (notes.length > 0) {
        callback(null, true);
      } else {
        callback(err);
      }
    });
  };

  get = (req, res) => {
    logger.info(this.entity, "getting customer with Id", req.params.id);
    this.model
      .findOne({ _id: req.params.id })
      .then(async obj => {
        try {
          if (obj.zohoId) {
            const acc = await zohoCRM.getAccount(obj.zohoId);
            obj = {
              ...obj.toObject(),
              street:
                acc["Billing Street"] == "null" ? "" : acc["Billing Street"],
              city: acc["Billing City"] == "null" ? "" : acc["Billing City"],
              state: acc["Billing State"] == "null" ? "" : acc["Billing State"],
              country:
                acc["Billing Country"] == "null" ? "" : acc["Billing Country"]
            };
            delete obj.ACCOUNTID;
          }
          return res.json(obj);
        } catch (err) {
          return res.json(obj);
        }
      })
      .catch(err => {
        logger.error(this.entity, `Error while retrieving customers`, err);
        console.error(err);
      });
  };

  getAll = (req, res) => {
    const query = req.query.query;
    if (query) {
      this.model
        .find({ name: { $regex: new RegExp(query, 'i') } })
        .then(customers => res.json({ customers }))
        .catch(err => {
          logger.error(this.entity, `Error while retrieving customers`, err);
          console.error(err);
        });
    } else {
      const criteria = {};
      const perPage = 20;
      const search = req.query.search;
      if (search) {
        criteria["name"] = { $regex: new RegExp(search, 'i') };
      }
      const page = Math.max(1, req.query.page) - 1;
      logger.info(this.entity, "getting all customers");
      this.model
        .find(criteria)
        .slice({ discounts: -1 })
        .limit(perPage)
        .skip(perPage * page)
        .sort({ createdAt: 1 })
        .exec()
        .then(docs => {
          this.model
            .find(criteria)
            .count()
            .exec()
            .then((count: number) => {
              res.json({
                customers: docs,
                page: page + 1,
                pages: parseInt(`${count}`)
              });
            });
        })
        .catch(err => {
          logger.error(this.entity, `Error while retrieving customers`, err);
          console.error(err);
        });
    }
  };

  insert = (req, res) => {
    logger.info(this.entity, "insert customer");
    let customer = req.body;
    if (customer.discounts !== "") {
      customer.discounts = [
        {
          discount: customer.discounts
        }
      ];
    } else {
      delete customer.discounts;
    }
    if (!customer.vat) {
      delete customer.vat;
    }
    customer = new this.model(customer);
    customer
      .save()
      .then(data => {
        return res.status(201).json(data);
      })
      .catch(err => {
        logger.error(this.entity, `Error while inserting customer`, err);
        return console.error(err);
      });
  };

  update = (req, res) => {
    logger.info(this.entity, "update customer", req.params.id);
    const body = req.body;
    this.model
      .findOneAndUpdate(
        { _id: req.params.id },
        {
          $set: { name: body.name, vat: body.vat },
          $push: { discounts: { discount: body.discounts } }
        }
      )
      .then(data => {
        res.status(200).end();
      })
      .catch(err => {
        logger.error(this.entity, `Error while updating customer`, err);
        res.status(500).json({ error: { msg: err.message } });
      });
  };

  getProjects = async (req, res) => {
    logger.info(this.entity, "get customer projects", req.params.id);
    const criteria = { customer: req.params.id };
    if (req.user.role === "subsidiary") {
      const subsidiaryUsers = (await this.userModel.find({
        subsidiary: req.user.subsidiary
      })).map(user => user._id.toString());
      criteria["createdBy"] = { $in: subsidiaryUsers };
    }
    this.projectModel
      .find(criteria)
      .populate([
        { path: "createdBy" },
        {
          path: "createdBy",
          select: "firstName lastName",
          populate: { path: "subsidiary" }
        }
      ])
      .then(projects => res.json(projects))
      .catch(err => {
        logger.error(
          this.entity,
          `Error while getting customer's projects`,
          err
        );
        console.error(err);
      });
  };

  getOrCreateCustomer = async (req, res, next) => {
    logger.info(this.entity, "get/create customer", req.params.id);
    const project = req.body;
    let customer = await this.model.findOne({ name: project.customer });
    if (customer) {
      project.customer = customer._id;
      req.customer = customer;
      return next();
    } else {
      zohoCRM
        .createCustomer({ "Account Name": project.customer })
        .then(data => {
          return this.model.create({
            name: project.customer,
            zohoId: data
          });
        })
        .then(data => {
          project.customer = data._id;
          req.customer = data;
          return next();
        })
        .catch(err => {
          logger.error(this.entity, `Error while get/create customer`, err);
          console.error(err);
        });
    }
  };
}
