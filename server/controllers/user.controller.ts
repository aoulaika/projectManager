import * as jwt from "jsonwebtoken";

import * as mailer from "../services/mailer/mailer";
import User from "../models/user.model";
import BaseCtrl from "./base";
import { nextTick } from "../../node_modules/@types/q";

export default class UserCtrl extends BaseCtrl {
  model = User;

  getAll = (req, res) => {
    let criteria = {},
      populate = [{ path: "subsidiary", select: "name" }];
    const admin = req.query.admin;
    if (admin != undefined) {
      criteria = { role: { $ne: "admin" } };
      populate = [];
    }
    this.model
      .find(criteria)
      .populate(populate)
      .then(docs => res.json(docs))
      .catch(err => console.error(err));
  };

  insert = (req, res) => {
    const obj = new this.model(req.body);
    let password = `${obj.firstName.toLowerCase()}${obj.lastName.toLowerCase()}${new Date().getFullYear()}`;
    password = password.replace(/\s/g, "");
    obj.password = password;
    obj
      .save()
      .then(item => {
        mailer.createUser({
          email: obj.email,
          password,
          name: `${obj.firstName} ${obj.lastName}`
        });
        res.status(201).json(item);
      })
      .catch(err => {
        if (err && err.code === 11000) {
          res.status(400).end();
        } else {
          res.status(400).end();
        }
      });
  };

  login = (req, res) => {
    this.model.findOne({ email: req.body.email }, (err, user) => {
      if (!user) {
        return res.sendStatus(403);
      }
      user.comparePassword(req.body.password, (error, isMatch) => {
        if (!isMatch) {
          return res.sendStatus(403);
        }
        const token = jwt.sign({ user: user }, process.env.SECRET_TOKEN); // , { expiresIn: 10 } seconds
        res.status(200).json({ token: token });
      });
    });
  };

  getById = id => {
    return this.model.findOne({ _id: id });
  };

  getByEmail = email => {
    return this.model.findOne({ email: email });
  };

  put = (req, res) => {
    this.model
      .findOneAndUpdate(
        { _id: req.params.id },
        {
          $set: {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            phone: req.body.phone
          }
        }
      )
      .exec()
      .then(docs => {
        res.status(200).json({ success: true, message: "update" });
      })
      .catch(err => console.error(err));
  };


  changeStatus = async (req, res, next) => {
    this.model.update({ email: req.body.email || req.user.email }, { isActive: req.body.isActive },(err, numAffected) => {
        if (err || numAffected ==0) {
          res.status(401);
          res.end();
        } else {
          res.status(200).json({ success: true, message: "update" });
          next();
        }
      });
  };

  isAdmin = async (req, res, next) => {
    let { user } = req;

    // Only admin can change users status
    if(user.role !== "admin") {
      res.status(401);
      res.end();
    }

    next();
  };

  isAllowed = async (req, res, next) => {
    let { user } = req;
    if(!user.isActive) {
      console.error("disactivated user")
      res.status(401);
      res.end();
    }
    next();
  };



  getOne = (req, res, next) => {
    this.model
      .findOne({ email: req.body.email || req.user.email })
      .exec((err, user) => {
        if (err || !user) {
          res.status(401);
          res.end();
        } else {
          user.comparePassword(req.body.password, (error, isMatch) => {
            if (error || !isMatch) {
              res.status(401).end();
            } else {
              req.user = user;
              next();
            }
          });
        }
      });
  };

  getProfile = (req, res) => {
    this.model
      .findOne({ _id: req.user._id })
      .then(profile => {
        res.json(profile);
      })
      .catch(err => console.error(err));
  };

  updateProfile = (req, res) => {
    const profile = req.body;
    this.model
      .update({ _id: req.user._id }, { $set: profile })
      .then(p => {
        res.json(p);
      })
      .catch(err => console.error(err));
  };

  getEmailById = (ids: string[]) => {
    return new Promise((resolve, reject) => {
      this.model
        .find({ _id: { $in: ids } }, { email: 1, _id: 0 })
        .then(docs => {
          const emails = docs.map(doc => doc.email).join(",");
          resolve(emails);
        })
        .catch(err => reject(err));
    });
  };

  getEmailsByRole = (
    criteria:
      | { role?: "service" | "manager" | "subsidiary"; subsidiary?: string }
      | any
  ) => {
    return new Promise((resolve, reject) => {
      this.model
        .find(criteria, { email: 1, _id: 0 })
        .then(docs => {
          const emails = docs.map(doc => doc.email).join(",");
          resolve(emails);
        })
        .catch(err => {
          reject(err);
        });
    });
  };
}
