import BaseCtrl from './base';
import Profile from '../models/profile.model';


export default class ProfileCtrl extends BaseCtrl {
  model = Profile;

  update = (req, res) => {
    const body = req.body;
    const promises = Object.keys(body)
      .map(label => this.model.update({label: label}, {$set: {budget: body[label]}}));
    Promise.all(promises)
      .then(data => {
        res.status(200).end();
      })
      .catch(err => console.error(err));
  }
}
