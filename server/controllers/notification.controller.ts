import Notification from '../models/notification.model';
import User from '../models/user.model';
import BaseCtrl from './base';


export default class NotificationCtrl extends BaseCtrl {
  model = Notification;
  userModel = User;

  getAll = (req, res) => {
    const criteria = {};
    if (req.user.role === "subsidiary") {
      criteria["scope"] = req.user.subsidiary;
    }
    this.model
      .find(criteria, null, {sort: {createdAt: -1}})
      .then(notifications => {
        res.json(notifications);
      })
      .catch(err => console.error(err));
  }
}
