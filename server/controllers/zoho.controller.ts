import * as request from "request";
import * as moment from "moment";

import * as mailer from "../services/mailer/mailer";
import Profile from "../models/profile.model";
import Resource from "../models/resource.model";
import * as zohoProject from "../services/zoho";
import * as zohoCRM from "../services/zoho-crm";

export default class ZohoCtrl {
  url = "https://projectsapi.zoho.com/restapi/portal/";
  PORTALID = "ayoubolk";
  params = { authtoken: "900202c9e5ac023d6d66befc04a7ea85" };
  model = Resource;
  profileModel = Profile;

  getUsers = (req, res) => {
    this.PORTALID = "wakandaio";
    this.params = { authtoken: "8e20ba9439e6d5c26e3d09d1d726067a" };
    request.get(
      {
        url: this.url + this.PORTALID + "/users/",
        qs: this.params,
        json: true
      },
      (err, response, body) => {
        if (err) {
          console.error(err);
        } else {
          const resources = body.users
            .filter(
              user => user.role !== "client" && user.role !== "contractor"
            )
            .map(user => {
              return {
                name: user.name,
                email: user.email,
                role: user.role,
                zohoId: user.id
              };
            });
          this.model.insertMany(resources, (error, docs) => {
            res.json(docs);
          });
        }
      }
    );
  };

  createProject = (req, res) => {
    const user = req.user;
    zohoProject
      .addProject(req.project.name)
      .then(async data => {
        const profilesBudget = await this.profileModel.find();
        const projectProfiles = req.project.profiles;
        let amount = profilesBudget.reduce(
          (acc, profile) =>
            acc + profile.budget * projectProfiles[profile.label],
          0
        );
        amount *= 1 - (req.project["discount"] || 0) / 100;
        amount += 1 + (req.project["customer"]["vat"] || 0) / 100;
        zohoCRM.potentialClosedWin(req.project.potentialId, amount);
        const zohoId = data[0].id_string;
        req.project.zohoId = zohoId;
        req.project.status.push({ label: "in-progress", date: Date.now() });
        zohoProject.createTasks(zohoId);
        zohoProject.createBackLogTaskList(zohoId).then(tasklists => {
          if (req.project.features.length > 0) {
            zohoProject.createBackLogTasks(
              zohoId,
              tasklists[0].id_string,
              req.project.features
            );
          }
        });
        zohoProject
          .addResources(zohoId, req.project.resources)
          .then(async () => {
            let ownerId = req.project.resources.filter(
              resource => resource.role === "manager"
            );
            if (ownerId.length > 0) {
              ownerId = ownerId[0].resource.zohoId.toString();
              zohoProject.addMilestones(zohoId, ownerId, req.project.sprints);
            }
          });
        req.project.save().then(() => {
          const data = { name: req.project.name, id: req.project._id, createdBy: req.project.createdBy };
          mailer.projectValidated(data, {role: user.role}, req.project.stakeholders);
          res.json({});
        });
      })
      .catch(err => console.error(err));
  };

  createPotential = (req, res, next) => {
    const project = req.body;
    zohoCRM
      .createPotential({
        "Potential Name": project.name,
        ACCOUNTID: `${req.customer.zohoId}`
      })
      .then(data => {
        project.potentialId = data;
        return next();
      })
      .catch(err => console.error(err));
  };

  potentialNegotiation = (req, res, next) => {
    zohoCRM.potentialNegotiation(req.project.potentialId);
    req.data["budgeted"] = true;
    mailer.budgeting({name: req.project.name, id: req.project._id, createdBy: req.project.createdBy}, req.project.stakeholders)
    return res.status(201).json(req.data);
  };

  createCustomer = (req, res, next) => {
    let customer = req.body;
    zohoCRM
      .createCustomer({
        "Account Name": customer.name,
        Phone: customer.phone,
        Fax: customer.fax
      })
      .then(data => {
        req.body["zohoId"] = data;
        next();
      })
      .catch(err => {
        console.log(
          `Error while creating ${customer.name} Account on Zoho CRM`
        );
        next();
      });
  };

  getAccountContacts = (req, res) => {
    zohoCRM
      .getAccountContacts(req.params.id)
      .then(data => {
        res.json({ contacts: data });
      })
      .catch(err => console.error(err));
  };

  test = (req, res) => {
    // zohoCRM
    //   .createDeal({
    //     "Potential Name": "test 1",
    //     "ACCOUNTID": "2959938000000176004",
    //     "Stage": "Open",
    //     "Closing Date": "01/05/2017"
    //   })
    //   .then(data => {
    //     console.log(JSON.stringify(data[0]));
    //     res.json(data[0]);
    //   })
    //   .catch(err => console.error(err));
    zohoCRM
      .potentialClosedWin("2959938000000154032", 1)
      .then(data => {
        console.log(JSON.stringify(data["data"]));
        res.json({});
      })
      .catch(err => console.error(err));
  };
}
