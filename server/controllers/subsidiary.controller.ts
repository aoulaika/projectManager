import BaseCtrl from './base';
import Subsidiary from '../models/subsidiary.model';


export default class SubsidiaryCtrl extends BaseCtrl {
  model = Subsidiary;

  getAll = (req, res) => {
    this.model.find()
      .then(subsidiaries => {
        res.json(subsidiaries);
      })
      .catch(err => console.error(err));
  }

  create = (req, res) => {
    const body = req.body;
    this.model.create(body)
      .then(subsidiary => {
        res.json(subsidiary);
      })
      .catch(err => console.error(err));
  }
}
