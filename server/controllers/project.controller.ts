import * as fs from "fs";
import * as Excel from "exceljs";
import * as randomColor from "randomcolor";

import logger from "../services/logger";
import * as mailer from "../services/mailer/mailer";
import { getEndDate } from "../services/utils";
import * as zohoCRM from "../services/zoho-crm";

import BaseCtrl from "./base";
import User from "../models/user.model";
import Project from "../models/project.model";
import Profile from "../models/profile.model";
import Customer from "../models/customer.model";
import { nextTick } from "q";

export default class ProjectCtrl extends BaseCtrl {
  model = Project;
  customerModel = Customer;
  profileModel = Profile;
  userModel = User;

  getByName = name => {
    return this.model.findOne({ name: name });
  };

  create = (req, res) => {
    const user = req.user;
    const project = req.body;
    const customer = req.customer;
    if ("notes" in project) {
      project.notes[0]["user"] = req.user._id;
    }
    project.status = [{ label: "new" }];
    project.createdBy = user._id;
    project.step = [];

    if ("discounts" in customer && customer.discounts && customer.discounts.length !== 0) {
      project["discount"] = customer.discounts.pop().discount;
    }

    // Attach profilesBudget to the project


    let pofilesBudget = [];
    Profile.find({})
      .exec()
      .then(docs => {
        docs.map((doc) => {
          pofilesBudget.push({label: doc.label, budget: doc.budget});
        });
        project.profilesBudget = pofilesBudget;

        this.model.create(project).then(obj => {
          const url = `https://services.4d.com/projects/${obj._id}`;
          mailer.createProject(
            {
              name: project.name,
              owner: `${user.firstName} ${user.lastName}`,
              url
            },
            project.stakeholders
          );
          res.json(obj);
        })
        .catch(err => console.error(err));
      })
      .catch((err) => {
        console.error(err);
        res.status(400).json({"message": "Something went wrong in addProfilesBudget middelware"});
      });
  };

  getAll = async (req, res) => {
    const criteria = {};
    if (req.user.role === "subsidiary") {
      const subsidiaryUsers = (await this.userModel.find({
        subsidiary: req.user.subsidiary
      })).map(user => user._id.toString());
      criteria["$or"] = [];
      criteria["$or"].push({"createdBy" : { $in: subsidiaryUsers }});
      criteria["$or"].push({"stakeholders.subsidiary" : req.user._id.toString()});
    }
    console.log("criteria", criteria);
    this.model
      .find(criteria)
      .select("name description status types step profilesBudget profiles discount total ")
      .slice({ status: -1 })
      .sort('-createdAt')
      .populate([
        {
          path: "createdBy",
          select: "firstName lastName",
          populate: { path: "subsidiary" }
        },
        {
          path: "customer",
          select: "name vat"
        }
      ])
      .exec()
      .then(docs => {
        res.json(docs);
      })
      .catch(err => console.error(err));
  };

  getOne = (req, res, next) => {
    this.model
      .findOne({ _id: req.params.projectId })
      .populate([
        { path: "resources.resource" },
        { path: "customer" },
        { path: "validations.history.user" },
        { path: "gdpr" }
      ])
      .then(project => {
        req.project = project;
        next();
      })
      .catch(err => {
        console.log(err);
        res.status(500).end();
      });
  };

  isAuthorized = (req, res, next) => {
    const { validations } = req.project;
    if (validations.length === 0) {
      return next();
    }
    const { history } = validations.slice(-1).pop();
    if (history.length) {
      const authorized = history.filter(item => !item.validation).length === 1;
      if (authorized) {
        return next();
      } else {
        return res.status(403).end();
      }
    }
  };

  get = (req, res) => {
    this.model
      .findOne({ _id: req.params.projectId })
      .populate([
        { path: "createdBy" },
        { path: "customer" },
        { path: "resources.resource" },
        { path: "stakeholders.subsidiary", select: "firstName lastName" },
        { path: "stakeholders.service", select: "firstName lastName" },
        { path: "stakeholders.manager", select: "firstName lastName" },
        { path: "notes.user", select: "firstName lastName" },
        { path: "validations.history.user" },
        { path: "gdpr" }
      ])
      .exec()
      .then(data => {
        const project = data.toJSON();
        const stakeholders = ["subsidiary", "service", "manager"];
        if (project.hasOwnProperty("stakeholders")) {
          stakeholders.forEach(holder => {
            if (project.stakeholders.hasOwnProperty(holder)) {
              project.stakeholders[holder] = `${project.stakeholders[holder][
                "firstName"
              ] || ""} ${project.stakeholders[holder]["lastName"] || ""}`;
            } else {
              project.stakeholders[holder] = "";
            }
          });
        } else {
          project["stakeholders"] = {};
          stakeholders.forEach(holder => {
            project["stakeholders"][holder] = "";
          });
        }
        res.json(project);
      })
      .catch(err => console.error(err));
  };

  update = async (req, res) => {
    const project = req.body;
    if ("customer" in project) {
      let customer = await this.customerModel.findOne({ name: project.customer });
      if (customer) {
        project.customer = customer._id;
      } else {
        customer = await this.customerModel.create({ name: project.customer });
        project.customer = customer._id;
      }
    }
    this.model
      .update(
        { _id: req.params.projectId },
        {
          $set: project
        }
      )
      .then(obj => {
        res.status(200).json({});
      })
      .catch(err => {
        res.status(500).end();
      });
  };

  updateStatus = (req, res) => {
    const { status } = req.project;
    const isCanceled = status.filter(s => s.label === "canceled").length > 0;
    if (isCanceled) {
      return res
        .status(409)
        .json({ error: { message: "cannot change project status" } });
    }
    const isInProgress =
      status.filter(s => s.label === "in-progress").length > 0;
    if (isInProgress && req.params.status === "canceled") {
      return res
        .status(409)
        .json({ error: { message: "cannot cancel in-progress project" } });
    }
    if (req.params.status === "resume") {
      const lastStatus = status.slice(-2).shift().label;
      req.project.status.push({ label: lastStatus, date: Date.now() });
    } else {
      req.project.status.push({ label: req.params.status, date: Date.now() });
      zohoCRM.potentialClosedLost(req.project.potentialId);
    }
    req.project
      .save()
      .then(data => res.json(data))
      .catch(err => console.error(err));
  };

  exists = (id, callback) => {
    this.model.count({ _id: id }, function(err, count) {
      if (count > 0) {
        callback(null, true);
      } else {
        callback(err);
      }
    });
  };

  addProfiles = (req, res, next) => {
    const newProfiles = req.body;
    const project = req.project;
    let resources = project.resources;
    const profiles = project.profiles;
    const isBudgeted =
      project.status.filter(s => s.label === "budgeted").length === 1;
    if (!isBudgeted) {
      project.status.push({
        label: "budgeted",
        date: Date.now()
      });
    }
    const changedProfiles = Object.keys(newProfiles).reduce((diff, key) => {
      if (profiles[key] === newProfiles[key]) {
        return diff;
      }
      return {
        ...diff,
        [key]: newProfiles[key]
      };
    }, {});
    const count = {};
    Object.keys(changedProfiles).forEach(p => {
      const sum = resources.reduce((s, r) => (r.role === p ? s + 1 : s), 0);
      if (sum > 0) {
        count[p] = sum;
      }
    });
    Object.keys(count).forEach(p => {
      resources = resources.map(r => {
        if (r.role === p) {
          r.budget = newProfiles[p] / count[p];
        }
        return r;
      });
    });
    project.profiles = newProfiles;
    project
      .save()
      .then(obj => {
        if (!isBudgeted) {
          req.data = obj;
          return next();
        } else {
          res.status(201).json(obj);
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).end();
      });
  };

  upload = (req, res) => {
    const attachments = req.files.map(file => file.originalname);
    this.model.update(
      { _id: req.params.projectId },
      {
        $addToSet: {
          attachments: { $each: attachments }
        }
      },
      (err, obj) => {
        if (err) {
          res.status(500).end();
        } else {
          res.json(attachments);
        }
      }
    );
  };

  removeFile = (req, res) => {
    fs.unlink(`${process.env.UPLOAD_DIR}/${req.params.fileName}`, err => {
      if (err) {
        res
          .status(500)
          .json({ msg: `error while removing file ${req.params.fileName}` });
      } else {
        this.model
          .update(
            { _id: req.params.projectId },
            { $pull: { attachments: req.params.fileName } }
          )
          .then(obj => {
            res.status(204).end();
          })
          .catch(err => {
            res.json(req.params.fileName);
          });
      }
    });
  };

  downloadFile = (req, res) => {
    res.download(`${process.env.UPLOAD_DIR}/${req.params.fileName}`);
  };

  getProjectsPlanning = async (req, res) => {
    const criteria = {};
    if (req.user.role === "subsidiary") {
      const subsidiaryUsers = (await this.userModel.find({
        subsidiary: req.user.subsidiary
      })).map(user => user._id.toString());
      criteria["createdBy"] = { $in: subsidiaryUsers };
    }
    this.model
      .find(criteria)
      .then(projects => {
        const data = projects
          .filter(
            project => project.status.length > 1 && project.startDate != null
          )
          .map(project => {
            let sum = project.sprints.reduce(
              (acc, sprint) => +sprint.duration + acc,
              0
            );
            if (sum === 0) {
              const budgets: {
                architect: number;
                mobile: number;
                backend: number;
                web: number;
                designer: number;
                tester: number;
                manager: number;
              } = project.resources.reduce(
                (acc, element) => {
                  acc[element.role] = element.budget;
                  return acc;
                },
                {
                  architect: 0,
                  mobile: 0,
                  backend: 0,
                  web: 0,
                  designer: 0,
                  tester: 0,
                  manager: 0
                }
              );
              sum =
                budgets.architect +
                budgets.designer +
                budgets.tester +
                budgets.manager +
                Math.max(budgets.mobile + budgets.backend + budgets.web);
            }
            const end = getEndDate(project.startDate, sum);
            return {
              id: project._id,
              title: project.name,
              start: project.startDate,
              allDay: true,
              color: randomColor({
                luminosity: "light",
                format: "rgb",
                hue: "blue"
              }),
              className: ["text-uppercase", "text-dark"],
              end: end
            };
          });
        res.json(data);
      })
      .catch(err => console.error(err));
  };

  getProjectsPlannings = async (req, res) => {
    const criteria = { _id: { $in: req.body.data } };
    if (req.user.role === "subsidiary") {
      const subsidiaryUsers = (await this.userModel.find({
        subsidiary: req.user.subsidiary
      })).map(user => user._id.toString());
      criteria["createdBy"] = { $in: subsidiaryUsers };
    }
    this.model
      .find({ _id: { $in: req.body.data } })
      .then(projects => {
        const data = projects
          .filter(
            project => project.status.length > 1 && project.startDate != null
          )
          .map(project => {
            let projectDuration = project.sprints.reduce(
              (sum, sprint) => +sprint.duration + sum,
              0
            );
            if (projectDuration === 0) {
              const budgets = project.profiles;
              projectDuration =
                budgets.architect +
                budgets.designer +
                budgets.tester +
                budgets.manager +
                Math.max(budgets.mobile, budgets.backend, budgets.web);
            }
            const end = getEndDate(project.startDate, projectDuration);
            return {
              id: project._id,
              title: project.name,
              start: project.startDate,
              allDay: true,
              color: randomColor({
                luminosity: "light",
                format: "rgb",
                hue: "blue"
              }),
              className: ["text-uppercase", "text-dark"],
              end: end
            };
          });
        res.json(data);
      })
      .catch(err => console.error(err));
  };

  exportExcel = async (req, res) => {
    const user = req.user;
    // get project details
    const project = await this.model
      .findOne({ _id: req.params.projectId })
      .populate([{ path: "resources.resource" }]);
    //const profiles = await this.profileModel.find();
    const profiles = project.profilesBudget;
    // create xslt workbook & set infos
    const workbook = new Excel.Workbook();
    workbook.creator = user.firstName + " " + user.lastName;
    // Features worksheet
    const featureSheet = workbook.addWorksheet("Features", {
      properties: {}
    });
    featureSheet.columns = [
      { header: "Feature", key: "name" },
      { header: "Description", key: "description" },
      { header: "Budget", key: "budget" }
    ];
    featureSheet.getRow(1).font = { bold: true };
    for (const feature of project["features"]) {
      featureSheet.addRow({
        name: feature.feature,
        description: feature.description,
        budget: parseFloat(feature.budget).toFixed(2)
      });
    }
    // Profiles worksheet
    const profileSheet = workbook.addWorksheet("Profiles", {
      properties: {}
    });
    profileSheet.columns = [
      { header: "Profiles", key: "profile" },
      { header: "Budget", key: "budget" }
    ];
    profileSheet.getRow(1).font = { bold: true };
    for (const profile of profiles) {
      if (project.profiles.hasOwnProperty(profile.label)) {
        profileSheet.addRow({
          budget: project.profiles[profile.label],
          profile: profile.label
        });
      }
    }
    // Team sheet
    const teamSheet = workbook.addWorksheet("Resources");
    teamSheet.columns = [
      { header: "Name", key: "name", width: 20 },
      { header: "Role", key: "role", width: 20 },
      { header: "Budget", key: "budget", width: 20 }
    ];
    teamSheet.getRow(1).font = { bold: true };
    for (const resource of project["resources"]) {
      teamSheet.addRow({
        name: resource.resource.name,
        role: resource.role,
        budget: parseFloat(resource.budget).toFixed(2)
      });
    }
    // Financial sheet
    const financialSheet = workbook.addWorksheet("Financials");
    financialSheet.addRow();
    financialSheet.addRow(["Profiles"]);
    financialSheet.getRow(2).font = { bold: true };
    financialSheet.addRow();
    financialSheet.addRow(["Profile", "Quantity", "Unit price", "Price"]);
    financialSheet.columns = [
      { key: "profile" },
      { key: "quantity" },
      { key: "unitPrice" },
      { key: "price" }
    ];
    financialSheet.getRow(4).font = { bold: true };
    for (const profile of profiles) {
      if (project.profiles.hasOwnProperty(profile.label)) {
        financialSheet.addRow({
          profile: profile.label,
          quantity: project.profiles[profile.label],
          unitPrice: profile.budget,
          price: profile.budget * project.profiles[profile.label]
        });
      }
    }
    financialSheet.addRow();
    financialSheet.addRow(["Sprints"]);
    financialSheet.getRow(4 + profiles.length + 2).font = { bold: true };
    financialSheet.addRow();
    workbook.xlsx.writeFile("budgeting.xlsx").then(function() {
      res.download("budgeting.xlsx");
    });
  };

  getProjectProfiles = async (req, res) => {
    this.model
      .findOne({ _id: req.params.projectId })
      .then(project => {
        res.status(200).json(project.profilesBudget);
      })
      .catch(err => {
        console.log(err);
        res.status(500).end();
      });
  };
}
