import * as mongoose from 'mongoose';
import BaseCtrl from './base';
import Project from '../models/project.model';

export default class NoteCtrl extends BaseCtrl {
  model = Project;

  exists = (noteId, projectId, callback) => {
    this.model.findOne({_id: projectId}, function (err, res){
      const notes = res.notes.filter(note => note._id.toString() === noteId);
      console.log(notes.length);
      if (notes.length > 0) {
        callback(null, true);
      } else {
        callback(err);
      }
    });
  }

  // Project notes management
  addNote = (req, res) => {
    const user = req.user;
    const body = req.body;
    const note = {
      _id: new mongoose.Types.ObjectId,
      note: body.note,
      date: Date.now(),
      user: user._id
    };
    this.model.update({_id: req.params.projectId}, {
      $push: {
        notes: note
      }
    }, (err, obj) => {
      if (err) {
        res.status(500).end();
      } else {
        note['user'] = {
          _id: user._id,
          firstName: user.firstName,
          lastName: user.lastName,
        };
        res.json(note);
      }
    });
  }

  removeNote = (req, res) => {
    // TODO: modify response and manage exception
    this.model.update({_id: req.params.projectId}, {$pull: {notes: {_id: req.params.noteId}}}, (err, obj) => {
      if (err) {
        res.status(500).json({success: false});
      } else {
        res.json(obj);
      }
    });
  }
}
