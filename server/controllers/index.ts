import UserCtrl from './user.controller';
import AuthCtrl from './auth.controller';
import ProjectCtrl from './project.controller';
import CustomerCtrl from './customer.controller';
import ResourceCtrl from './resource.controller';
import ZohoCtrl from './zoho.controller';
import NoteCtrl from './note.controller';
import FeatureCtrl from './feature.controller';
import SubsidiaryCtrl from './subsidiary.controller';
import SprintCtrl from './sprint.controller';
import ProfileCtrl from './profile.controller';
import NotificationCtrl from './notification.controller';
import ValidationCtrl from './validation.controller';
import GdprCtrl from './gdpr.controller';

const userCtrl = new UserCtrl();
const authCtrl = new AuthCtrl();
const projectCtrl = new ProjectCtrl();
const featureCtrl = new FeatureCtrl();
const customerCtrl = new CustomerCtrl();
const resourceCtrl = new ResourceCtrl();
const zohoCtrl = new ZohoCtrl();
const noteCtrl = new NoteCtrl();
const subsidaryCtrl = new SubsidiaryCtrl();
const sprintCtrl = new SprintCtrl();
const profileCtrl = new ProfileCtrl();
const notificationCtrl = new NotificationCtrl();
const validationCtrl = new ValidationCtrl();
const gdprCtrl = new GdprCtrl();

const ctrls = {
  'userCtrl': userCtrl,
  'authCtrl': authCtrl,
  'projectCtrl': projectCtrl,
  'featureCtrl': featureCtrl,
  'customerCtrl': customerCtrl,
  'resourceCtrl': resourceCtrl,
  'zohoCtrl': zohoCtrl,
  'noteCtrl': noteCtrl,
  'subsidaryCtrl': subsidaryCtrl,
  'sprintCtrl': sprintCtrl,
  'profileCtrl': profileCtrl,
  'notificationCtrl': notificationCtrl,
  'validationCtrl': validationCtrl,
  'gdprCtrl': gdprCtrl
};

export default ctrls;
