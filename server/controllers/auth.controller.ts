import BaseCtrl from "./base";
import * as jwt from "jsonwebtoken";

import User from "../models/user.model";

export default class AuthCtrl extends BaseCtrl {
  model = User;
  changePassword = (req, res) => {
    const obj = new this.model(req.user);
    obj.password = req.body.newPassword;
    obj
      .save()
      .then(user => {
        res.status(204).end();
      })
      .catch(err => console.error(err));
  };
  isLoggedIn = (req, res, next) => {
    if (req.user) {
      next();
    } else {
      res.status(401).json({ message: "Unauthorized user!" });
    }
  };
  isAuthorized = (req, res, next) => {
    return next();
  };
  createJWT = (req, res) => {
    const options = { expiresIn: "1d" };
    const body = req.body;
    if (body.hasOwnProperty("rememberMe") && body.rememberMe) {
      delete options.expiresIn;
    }
    const token = jwt.sign(
      { user: req.user.toJSON() },
      process.env.SECRET_TOKEN,
      options
    );
    res.cookie("Session", token);
    res.status(200);
    return res.json({ token: token });
  };
}
